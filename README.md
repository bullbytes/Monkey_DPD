# Monkey DPD
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/a0961e228c1e4d84b30eb811f6fa2953)](https://www.codacy.com/app/bullbytes/Monkey_DPD?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=bullbytes/Monkey_DPD&amp;utm_campaign=Badge_Grade)

With this program, users can print [DPD](https://www.dpd.com/) parcel labels from [MonKey Office](https://www.monkey-office.de/). The program uses [DPD Print](https://esolutions.dpd.com/clientloesungen/dpdprint.aspx) to print parcel labels.

----
Monkey DPD converts delivery notes in MonKey Office to DPD parcel labels. Conversion currently works with delivery notes in German. If you are inclined to change this, please have a look at the [parsers](https://gitlab.com/bullbytes/Monkey_DPD/tree/master/src/main/java/com/bullbytes/parsing/parsers).

This is how printing delivery notes works:

1. MonKey Office sends via the print dialog a PDF with customer data to [`print DPD labels.sh`](https://gitlab.com/bullbytes/Monkey_DPD/blob/master/monkey_office_connector/print%20DPD%20labels.sh)
2. `print DPD labels.sh` calls `monkeyDpd.jar`
3. `monkeyDpd.jar` parses the PDF and inserts the customer data into a database on a (remote) machine running DPD Print
4. DPD Print polls this database for new data: Once there's a new entry in the database, DPD Print prints the data out as a parcel label

The setup described below was successfully used with MonKey Office 2016 version 13.2.1 and DPD Print 6.1.0.1.

## DPD Print setup
1. Download and install [DPD Print](https://esolutions.dpd.com/clientloesungen/dpdprint.aspx) (also known as DELISprint).
2. [Set up the database and start it](#database-setup). We save a recipient's data from a MonKey Office delivery note into this database as an order. Once DPD Print finds a new order in the database, it will print the recipient's data as a parcel label.
3. In DPD Print, select `Data import` → `Configuration of import structure for orders` and associate DPD Print fields in the column `DPD Print` with the column names in your database in the column `Data source`. For example:
![import-structure-configuration](https://i.imgur.com/SFR5lYy.png)
4. Select `Configure service` and define in what interval DPD Print should check the database for new orders
5. Hit `Start import service`
6. Make sure you've selected the right printer

## MonKey Office setup for Mac
This setup is done at the machine running MonKey Office.

1. Install the [Java Development Kit](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
2. Put the script in `monkey_office_connector` in `~/Library/PDF Services` to make it show up in print dialogs on a Mac. See also [PDF workflows on Mac](https://developer.apple.com/library/mac/documentation/Printing/Conceptual/PDF_Workflow/pdfwf_concepts/pdfwf_concepts.html#//apple_ref/doc/uid/TP30000167-DontLinkElementID_4).
3. Make the script executable `chmod +x "print DPD labels.sh"`. Now the user can right-click a delivery note in MonKey Office and select `print DPD labels` in the print menu.
4. Put Monkey DPD's jar file in `/Users/Shared/monkeyDpd/`. See the [build instructions](#build-instructions) on how to compile Monkey DPD.
5. Make the jar file executable: `chmod +x monkeyDpd.jar`
6. Put the configuration file `monkeyDpd.properties` in `/Users/Shared/monkeyDpd/` and make sure the credentials for connecting to the database are correct. [Here](https://gitlab.com/bullbytes/Monkey_DPD/blob/master/monkey_office_connector/monkeyDpd.properties)'s an example configuration.

## Build instructions
How to create the `monkeyDpd.jar` which reads a delivery note from MonKey Office and sends it to the [database](#database-setup) for DPD Print to print a parcel label:

1. Clone this repository and `cd` into it
2. Execute `./gradlew createCapsule` to create a runnable jar using [Capsule](http://www.capsule.io/)
3. In order for the script in `monkey_office_connector` to call the created jar (as described in the [MonKey Office setup](#monkey-office-setup-for-mac)), copy the jar to the directory where the script expects it: `cp build/libs/monkeyDpd-0.0.1-capsule.jar /Users/Shared/monkeyDpd/monkeyDpd.jar`

## Database setup
This setup is done at the machine running the database that DPD Print checks for orders.

Clients will connect to this database using the URL defined in [monkeyDpd.properties](https://gitlab.com/bullbytes/Monkey_DPD/blob/master/monkey_office_connector/monkeyDpd.properties). Therefore it's necessary that the database can be reached and no firewall prohibits access. For example, if the database URL is `jdbc:h2:tcp://169.254.219.144:9092/testDb`, the machine running DPD Print must allow incoming connections on port 9092.

1. [Download H2 Console](http://www.h2database.com/h2-setup-2014-04-05.exe). Be sure to download version `1.3.176 (2014-04-05)`, since DPD Print 6.1.0.1 doesn't have the driver to handle newer versions of H2.

2. Set up the database using H2 Console:
   Select "Generic H2 (Server)" in the settings.
   For the "Driver Class", select `org.h2.Driver`
   The JDBC URL can be, for example, `jdbc:h2:tcp://localhost/~/h2_dbs/yourDb`

3. Create a table that stores orders for DPD:
```sql
/* The rows' IDs start at zero and are automatically incremented by one after each insertion.
   DPD Print needs the columns DP_STATUS and DP_OUTPUT. DPD Print stores error messages in DP_OUTPUT.
   The value of SHIPMENTTYPE has to be 'NP', or 'COD'.
   The value of RCOUNTRY has to conform to alpha-2 country codes (for example 'AT', or 'DE'. See https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2) */
CREATE TABLE ORDERS(ID INT NOT NULL IDENTITY(0,1) PRIMARY KEY, SHIPMENTTYPE VARCHAR(255), RNAME1 VARCHAR(255), RCONTACT VARCHAR(255), RSTREET VARCHAR(255), RCOUNTRY VARCHAR(255), RPOSTAL VARCHAR(255), RCITY VARCHAR(255), DP_STATUS VARCHAR(255), DP_OUTPUT CLOB);
```

4. Modify the H2 server start script at, for example, `C:\Program Files (x86)\H2\bin\h2w.bat` to allow remote TCP connections. Optionally, prohibit clients from creating new databases using `-ifExists` (but this might not what you want if you do intend to create a new database):
```bat
@start javaw -cp "h2-1.3.176.jar;%H2DRIVERS%;%CLASSPATH%" org.h2.tools.Console %* -tcpAllowOthers -baseDir ~/h2_dbs/
@if errorlevel 1 pause
```
DPD Print and all other applications using the database need H2 Console to be running in order to connect to the database.

