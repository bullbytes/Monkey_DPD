#!/bin/bash
#
# Place this script in "~/Library/PDF Services" to make it show up in print dialogs on a Mac.
#
# We expect that MonKey Office calls this script and that it provides three arguments:
#
#   1. The file name of a delivery note
#   2. Print options of a print job
#   3. The full path to the delivery note of the first argument
#
# These three arguments are provided to Unix tools when used in a PDF Workflow on Mac. When this script is placed in "~/Library/PDF Services", it shows up in the print dialog. Thus, a user of MonKey Office can right-click on a delivery note and select this script to turn a delivery note into a DPD parcel label.
# This script takes the third argument (if it exists) and passes it to another program called Monkey DPD which will create a DPD label from MonKey Office's delivery note and send it to DPD Print.
# See also: https://developer.apple.com/library/mac/documentation/Printing/Conceptual/PDF_Workflow/pdfwf_concepts/pdfwf_concepts.html#//apple_ref/doc/uid/TP30000167-DontLinkElementID_4

# Executables and configuration files for Monkey DPD are in this directory
monkeyDpdDir="/Users/Shared/monkeyDpd"
# This is Monkey DPD, the program we relay the full path of the delivery note. It parses the recipient data from the delivery note and makes DPD Print print the DPD label containing the data
monkeyDpdJar="$monkeyDpdDir/monkeyDpd.jar"
# Configuration needed for connecting to DPD Print and logging
monkeyDpdConfig="$monkeyDpdDir/monkeyDpd.properties"
# Monkey DPD will look for a system property with this name to get its configuration
monkeyDpdConfigVar="monkeyDpd.config"

# This script logs to this file
logFile="$monkeyDpdDir/monkey_office_connector.log"

function log {
  # Date and time format that complies to ISO 8601
  curDateAndTime="$(date +%Y-%m-%dT%H:%M:%S%z)";
  echo "$curDateAndTime: $1" >> $logFile
}
nrOfArguments=$#

log "Execution started"

if (( nrOfArguments >= 3 ))
then
  if [ -f "$monkeyDpdJar" ]
  then
    # The zeroth element is the name of this script, after that come the individual parameters
    deliveryNotePath=$3
    log "Executing $monkeyDpdJar with $deliveryNotePath. Configuration file is $monkeyDpdConfig"
    java -D"$monkeyDpdConfigVar"="$monkeyDpdConfig" -jar "$monkeyDpdJar" "$deliveryNotePath"
  else
    log "Can't execute $monkeyDpdJar since it doesn't exist"
  fi
else
  log "Error: Expected at least three arguments but got $nrOfArguments instead"
  for arg in "$@"
  do
    log "$arg"
  done
fi

