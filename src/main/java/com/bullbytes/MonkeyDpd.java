package com.bullbytes;

import com.bullbytes.dpdlabel.DpdLabel;
import com.bullbytes.database.DpdPrintConnector;
import com.bullbytes.database.properties.DatabaseProperties;
import com.bullbytes.database.properties.DatabasePropertiesProvider;
import com.bullbytes.parsing.parsers.DpdLabelParser;
import com.bullbytes.results.DatabaseResult;
import com.bullbytes.results.MonkeyDpdResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.Collection;
import java.util.Properties;

import static com.bullbytes.results.MonkeyDpdResult.fromDatabaseResult;

/**
 * Monkey DPD reads the PDF file passed to it, generates a {@link DpdLabel} from the file, and sends the label to a
 * database. <a href="https://esolutions.dpd.com/clientloesungen/dpdprint.aspx">DPD Print</a> can be configured to
 * watch this database for new labels and print them out.
 * <p>
 * Created by Matthias Braun on 17/08/16.
 */
public final class MonkeyDpd {

    private static final Logger log = LoggerFactory.getLogger(MonkeyDpd.class);
    private final Properties properties;

    private MonkeyDpd(Properties properties) {
        this.properties = properties;
    }


    /**
     * Starts {@link MonkeyDpd}. It will attempt to convert the MonKey Office {@code delivery note} into
     * a {@link DpdLabel} and send it to the database defined in the {@code properties}.
     *
     * @param deliveryNote this file is treated as a MonKey Office delivery note; we'll create a {@link DpdLabel} from
     *                     it
     * @param properties   configures {@link MonkeyDpd} and contains, for example, the location of the database to which
     *                     we'll save the {@link DpdLabel}
     */
    static MonkeyDpdResult start(File deliveryNote, Properties properties) {

        log.info("MonkeyDPD started");

        MonkeyDpd monkeyDpd = new MonkeyDpd(properties);

        MonkeyDpdResult monkeyResult = DpdLabelParser.parse(deliveryNote).fold(
                MonkeyDpd::resultFromFailure,
                labels -> fromDatabaseResult(monkeyDpd.sendDpdLabels(labels))
        );

        log.info("{}", monkeyResult);

        return monkeyResult;
    }

    private static MonkeyDpdResult resultFromFailure(Exception failure) {
        return MonkeyDpdResult.fromException("Could not parse labels.", failure);
    }

    private DatabaseResult sendDpdLabels(Collection<DpdLabel> dpdLabels) {
        DatabaseProperties dbProperties = DatabasePropertiesProvider.getDatabasePropertiesFrom(properties);
        DpdPrintConnector dpdPrint = DpdPrintConnector.withConfig(dbProperties);
        return dpdPrint.sendLabelsToDpdPrint(dpdLabels);
    }
}
