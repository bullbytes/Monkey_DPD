package com.bullbytes;

import com.bullbytes.database.properties.DatabasePropertiesProvider;
import com.bullbytes.results.MonkeyDpdResult;
import com.bullbytes.utils.FileUtil;
import com.bullbytes.utils.MonkeyDpdSystemProperty;
import com.google.common.base.Charsets;
import com.google.common.io.Files;
import io.atlassian.fugue.Either;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.util.Properties;

/**
 * Reads the properties file passed to this jar as a {@link MonkeyDpdSystemProperty system property}.
 */
public final class Starter {

    private static final Logger log = LoggerFactory.getLogger(Starter.class);

    private Starter() {
    }

    /**
     * Parses the main methods arguments to get the PDF of the MonKey Office delivery note that we want to turn into a
     * DPD parcel label.
     * <p>
     * We expect that this application is provided one argument, the full path to the delivery note.
     *
     * @return {@link Either either} the delivery note as a PDF {@link File} ot the {@link Exception} that occurred
     */
    private static Either<Exception, File> getPdfFile(String... args) {
        Either<Exception, File> exceptionOrFile;
        if (args.length >= 1) {
            exceptionOrFile = Either.right(new File(args[0]));
        } else {
            String errorMsg = "Expected at least one argument, got " + args.length + " instead";
            exceptionOrFile = Either.left(new IllegalArgumentException(errorMsg));
        }
        return exceptionOrFile;
    }

    /**
     * Starts {@link MonkeyDpd} that reads customer data from a MonKey Office delivery note and sends that
     * data to DPD Print so it prints a parcel label designating the customer as the recipient of the parcel.
     *
     * @param args we expect that this program is called like this:
     *             <p>
     *             {@code java -DmonkeyDpd.config="path/to/config.properties" -jar MonkeyDpd.jar path/to/deliveryNote.pdf}
     *             <p>
     *             The .properties files configures {@link MonkeyDpd} and the PDF file is converted to a DPD
     *             parcel label.
     *             <p>
     *             See {@link MonkeyDpdSystemProperty} for system properties that {@link MonkeyDpd} expects to be set.
     *             <p>
     *             See {@link DatabasePropertiesProvider} for the expected contents of the .properties file.
     */
    public static void main(String... args) {
        try {
            File deliveryNote = getPdfFile(args)
                    .rightOr(e -> {
                        log.warn("Exception while getting path to delivery note", e);
                        return new File("");
                    });
            Properties properties = readPropertiesFromFile(getPropertiesFile())
                    .rightOr(e -> {
                        log.warn("Exception while reading properties file", e);
                        return new Properties();
                    });
            MonkeyDpdResult result = MonkeyDpd.start(deliveryNote, properties);

            if (result.isSuccess()) {

                // Delete the PDF created by MonKey Office that we parsed
                boolean fileDeleted = FileUtil.delete(deliveryNote);

                log.info("Delivery note deleted: {}", fileDeleted);
            }
        } catch (Throwable e) {
            log.error("Error while executing Monkey DPD", e);
        }
    }

    private static Either<Exception, Properties> readPropertiesFromFile(File file) {
        Either<Exception, Properties> exceptionOrProperties;
        try (BufferedReader fileReader = Files.newReader(file, Charsets.UTF_8)) {

            Properties properties = new Properties();
            properties.load(fileReader);

            exceptionOrProperties = Either.right(properties);

        } catch (IOException e) {
            exceptionOrProperties = Either.left(e);
        }
        return exceptionOrProperties;
    }

    private static File getPropertiesFile() {
        return MonkeyDpdSystemProperty
                .get(MonkeyDpdSystemProperty.CONFIG_FILE)
                .map(File::new)
                .orElseGet(() -> {
                    log.warn("Please provide the path to the configuration file by calling this program with '-D{}=yourConfigFile.properties'",
                            MonkeyDpdSystemProperty.CONFIG_FILE);
                    return new File("");
                });
    }
}
