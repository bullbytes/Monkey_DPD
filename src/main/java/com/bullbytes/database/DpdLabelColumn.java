package com.bullbytes.database;

import com.bullbytes.dpdlabel.DpdLabel;

/**
 * The column names for a {@link DpdLabel} in the database of the
 * <a href="https://esolutions.dpd.com/clientloesungen/dpdprint.aspx">DPD Print</a> server.
 * <p>
 * Created by Matthias Braun on 11/08/16.
 */
enum DpdLabelColumn {
    RECIPIENT_NAME_1("RNAME1"), RECIPIENT_STREET("RSTREET"), RECIPIENT_COUNTRY("RCOUNTRY"),
    RECIPIENT_POSTAL_CODE("RPOSTAL"), RECIPIENT_CITY("RCITY"), SHIPMENT_TYPE("SHIPMENTTYPE"),
    RECIPIENT_PERSON_OF_CONTACT("RCONTACT");

    private final String columnName;

    DpdLabelColumn(String columnName) {
        this.columnName = columnName;
    }

    @Override
    public String toString() {
        return columnName;
    }
}
