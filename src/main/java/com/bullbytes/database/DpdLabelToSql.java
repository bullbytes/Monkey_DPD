package com.bullbytes.database;

import com.bullbytes.dpdlabel.DpdLabel;
import com.bullbytes.database.data.SqlRow;
import com.bullbytes.utils.DbUtil;
import com.bullbytes.utils.MapUtil;
import io.atlassian.fugue.Either;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;

/**
 * Knows how to convert a {@link DpdLabel} into an SQL statement for inserting it in a database watched by a
 * <a href="https://esolutions.dpd.com/clientloesungen/dpdprint.aspx">DPD Print</a> server.
 * DPD Print can be configured to print out these {@link DpdLabel}s when it sees new entries in the database.
 * <p>
 * Created by Matthias Braun on 11/08/16.
 */
final class DpdLabelToSql {
    private static final String TABLE_NAME = "ORDERS";

    private DpdLabelToSql() {
    }

    static Either<Exception, PreparedStatement> toInsertStatement(Collection<DpdLabel> dpdLabels, Connection connection) {
        List<SqlRow> rows = dpdLabels.stream()
                .map(DpdLabelToSql::toRow)
                .collect(toList());
        return DbUtil.createBatchInsert(TABLE_NAME, rows, connection);
    }

    private static SqlRow toRow(DpdLabel dpdLabel) {
        return SqlRow.fromColumnValueMap(getColumnNamesAndValues(dpdLabel));
    }

    private static Map<String, String> getColumnNamesAndValues(DpdLabel label) {
        Map<DpdLabelColumn, String> columnNamesAndValues = new HashMap<>();

        columnNamesAndValues.put(DpdLabelColumn.SHIPMENT_TYPE, label.getShipmentType());
        columnNamesAndValues.put(DpdLabelColumn.RECIPIENT_NAME_1, label.getRecipientName());
        columnNamesAndValues.put(DpdLabelColumn.RECIPIENT_STREET, label.getRecipientStreet());
        columnNamesAndValues.put(DpdLabelColumn.RECIPIENT_COUNTRY, label.getRecipientCountryCode());
        columnNamesAndValues.put(DpdLabelColumn.RECIPIENT_POSTAL_CODE, label.getRecipientPostalCode());
        columnNamesAndValues.put(DpdLabelColumn.RECIPIENT_CITY, label.getRecipientCity());
        label.getPersonOfContact()
                .ifPresent(personOfContact -> columnNamesAndValues.put(DpdLabelColumn.RECIPIENT_PERSON_OF_CONTACT, personOfContact));

        return MapUtil.keysToString(columnNamesAndValues);
    }

}
