package com.bullbytes.database;

import com.bullbytes.results.DatabaseResult;
import com.bullbytes.dpdlabel.DpdLabel;
import com.bullbytes.database.properties.DatabaseProperties;
import com.bullbytes.utils.DbUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.util.Collection;

/**
 * Connects to a <a href="https://esolutions.dpd.com/clientloesungen/dpdprint.aspx">DPD Print</a> server.
 * DPD Print can be configured to print labels as soon as it receives a new {@link DpdLabel}.
 * <p>
 * Created by Matthias Braun on 09/08/16.
 */
public final class DpdPrintConnector {

    private static final Logger log = LoggerFactory.getLogger(DpdPrintConnector.class);
    private final DatabaseProperties properties;

    private DpdPrintConnector(DatabaseProperties dbProperties) {
        properties = dbProperties;
    }

    public static DpdPrintConnector withConfig(DatabaseProperties dbProperties) {
        return new DpdPrintConnector(dbProperties);
    }

    private static DatabaseResult send(Collection<DpdLabel> dpdLabels, Connection connection) {

        int nrOfLabels = dpdLabels.size();
        String labelOrLabels = nrOfLabels == 1 ? "label" : "labels";
        log.info("Send {} DPD {} to '{}'", nrOfLabels, labelOrLabels, DbUtil.getUrl(connection).orElse("unknown URL"));

        return DpdLabelToSql.toInsertStatement(dpdLabels, connection)
                .map(DbUtil::executeUpdate)
                .fold(
                        DatabaseResult::fromException,
                        sendResult -> sendResult);
    }

    public DatabaseResult sendLabelsToDpdPrint(Collection<DpdLabel> dpdLabels) {
        return DbUtil.getH2Connection(properties).fold(
                DatabaseResult::fromException,
                connection -> send(dpdLabels, connection));
    }
}
