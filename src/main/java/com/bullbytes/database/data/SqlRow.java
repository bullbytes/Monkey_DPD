package com.bullbytes.database.data;

import com.bullbytes.utils.DbUtil;
import com.bullbytes.utils.MapUtil;
import com.bullbytes.utils.Strings;
import net.jcip.annotations.Immutable;

import java.sql.Connection;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import static java.util.stream.Collectors.toList;

/**
 * Used for inserting a row into a database via {@link DbUtil#createBatchInsert(String, List, Connection)}.
 * <p>
 * Created by Matthias Braun on 20/08/16.
 */
@Immutable
public final class SqlRow {
    private final SortedMap<String, String> values;

    private SqlRow(Map<String, String> columnsAndValues) {
        // TreeMap orders the entries by its keys
        values = new TreeMap<>(columnsAndValues);
    }

    /**
     * Creates an {@link SqlRow} from a map of column names to the values of the row. If you want to create an
     * {@link SqlRow} for persons, you might pass this method a map like this (the key is the column name):
     * <pre>
     *    firstName -> John, Jane
     *    lastName -> Doe, Digger
     *    age -> 32, 45
     * </pre>
     *
     * @param columnNamesAndValues the column names and the values associated with it as described above
     * @return an initialized {@link SqlRow} with the values of the given {@code columnNamesAndValues}
     */
    public static SqlRow fromColumnValueMap(Map<String, String> columnNamesAndValues) {
        return new SqlRow(columnNamesAndValues);
    }

    /**
     * @return the columns of this {@link SqlRow}, sorted alphabetically
     */
    public List<String> getColumns() {
        return values.keySet().stream().sorted().collect(toList());
    }

    /**
     * @return this {@link SqlRow}, where values are replaced with the placeholder used in prepared statements. For
     * example if this row has four values: {@code (?, ?, ?, ?)}
     */
    public String getValuePlaceholders() {
        int nrOfValues = values.size();
        return "(" + Strings.repeat(DbUtil.PLACEHOLDER, nrOfValues, ", ") + ")";
    }

    public List<String> getValues() {
        return MapUtil.getSortedValues(values, getColumns());
    }
}
