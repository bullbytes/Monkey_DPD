package com.bullbytes.database.properties;

import net.jcip.annotations.Immutable;

/**
 * Contains the credentials and the URL of the database we want to connect to.
 * <p>
 * Created by Matthias Braun on 16/08/16.
 */
@Immutable
public final class DatabaseProperties {
    private final String userName;
    private final String password;
    private final String url;

    private DatabaseProperties(String url, String userName, String password) {
        this.url = url;
        this.userName = userName;
        this.password = password;
    }

    static DatabaseProperties create(String url, String userName, String password) {
        return new DatabaseProperties(url, userName, password);
    }

    public String getPassword() {
        return password;
    }

    public String getUserName() {
        return userName;
    }

    public String getUrl() {
        return url;
    }
}
