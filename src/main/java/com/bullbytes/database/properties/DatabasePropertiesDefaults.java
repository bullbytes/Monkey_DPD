package com.bullbytes.database.properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Provides default values for connecting to the DPD Print database.
 * We use these values if the configuration file passed to this program could not be read (or no file was passed at all).
 * <p>
 * Created by Matthias Braun on 16/08/16.
 */
final class DatabasePropertiesDefaults {
    // Mind that DB URLs are not real URLs, meaning new URL("jdbc:..."); throws a MalformedURLException
    private static final String DEFAULT_DB_URL = "jdbc:h2:tcp://localhost:9092/testDb";
    private static final String DEFAULT_PASSWORD = "default password";
    private static final String DEFAULT_USER_NAME = "default user name";
    private static final Logger log = LoggerFactory.getLogger(DatabasePropertiesDefaults.class);

    private DatabasePropertiesDefaults() {
    }

    static String getDefaultFor(DatabasePropertyName propertyName) {
        final String defaultValue;
        switch (propertyName) {
            case URL:
                defaultValue = DEFAULT_DB_URL;
                break;
            case USER_PASSWORD:
                defaultValue = DEFAULT_PASSWORD;
                break;
            case USER_NAME:
                defaultValue = DEFAULT_USER_NAME;
                break;
            default:
                log.warn("Could not get default value for property: {}", propertyName);
                defaultValue = "default";
                break;
        }
        return defaultValue;
    }
}
