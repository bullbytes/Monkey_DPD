package com.bullbytes.database.properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

/**
 * Gets the {@link DatabaseProperties} from some {@link Properties}.
 * <p>
 * Created by Matthias Braun on 17/08/16.
 */
public final class DatabasePropertiesProvider {
    private static final Logger log = LoggerFactory.getLogger(DatabasePropertiesProvider.class);

    private DatabasePropertiesProvider() {
    }

    /**
     * Creates {@link DatabaseProperties} from the given {@code properties}.
     * <p>
     * We assume that the {@code properties} contains properties with the names in
     * {@link DatabasePropertyName}.
     *
     * @param properties we create {@link DatabaseProperties} from these {@link Properties}
     * @return the {@link DatabaseProperties} created from the {@code properties}
     */
    public static DatabaseProperties getDatabasePropertiesFrom(Properties properties) {

        String url = getOrWarn(DatabasePropertyName.URL, properties);
        String userName = getOrWarn(DatabasePropertyName.USER_NAME, properties);
        String password = getOrWarn(DatabasePropertyName.USER_PASSWORD, properties);

        return DatabaseProperties.create(url, userName, password);
    }

    private static String getOrWarn(DatabasePropertyName propertyName, Properties properties) {

        String value = properties.getProperty(propertyName.getVarName());
        if (value == null) {
            String defaultVal = DatabasePropertiesDefaults.getDefaultFor(propertyName);
            log.warn("Could not get value of property {}, using default of '{}' instead", propertyName, defaultVal);

            value = defaultVal;
        }
        return value;
    }
}
