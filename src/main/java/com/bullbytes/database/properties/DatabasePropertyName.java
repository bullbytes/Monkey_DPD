package com.bullbytes.database.properties;

/**
 * The name of a database property.
 * <p>
 * We expect properties with these names to be passed to Monkey DPD.
 */
enum DatabasePropertyName {
    URL("db.url"), USER_NAME("db.user.name"), USER_PASSWORD("db.user.password");

    private final String varNameInProperties;

    DatabasePropertyName(String varNameInProperties) {
        this.varNameInProperties = varNameInProperties;
    }

    public String getVarName() {
        return varNameInProperties;
    }
}
