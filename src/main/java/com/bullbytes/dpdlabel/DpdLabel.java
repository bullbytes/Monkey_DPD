package com.bullbytes.dpdlabel;

import org.inferred.freebuilder.FreeBuilder;

import java.util.Optional;

/**
 * Represents a label for shipping a parcel using Dynamic Parcel Distribution.
 * <p>
 * Created by Matthias Braun on 06/08/16.
 */
@FreeBuilder
public interface DpdLabel {

    String getShipmentType();

    String getRecipientName();

    String getRecipientStreet();

    String getRecipientCountryCode();

    String getRecipientCity();

    String getRecipientPostalCode();

    Optional<String> getPersonOfContact();

    Optional<String> getOrderNr();

    String getCustomerNr();

    Optional<String> getSalutation();

    Optional<String> getAcademicTitle();

    class Builder extends DpdLabel_Builder {
        public Builder() {
            setDefaults();
        }

        private void setDefaults() {
            setRecipientName(LabelDefaults.NAME);
            setShipmentType(LabelDefaults.SHIPMENT_TYPE);
            setCustomerNr(LabelDefaults.CUSTOMER_NR);
            setRecipientCity(LabelDefaults.CITY);
            setRecipientPostalCode(LabelDefaults.POSTAL_CODE);
            setRecipientStreet(LabelDefaults.STREET);
            setRecipientCountryCode(LabelDefaults.COUNTRY_CODE);
        }
    }
}
