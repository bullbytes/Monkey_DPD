package com.bullbytes.dpdlabel;

/**
 * The default values for a {@link DpdLabel}.
 * <p>
 * Created by Matthias Braun on 08/09/16.
 */
public final class LabelDefaults {
    public static final String POSTAL_CODE = "0000";
    public static final String CITY = "Unknown city";
    public static final String CUSTOMER_NR = "Unknown customer number";

    // The default shipment type is "NP" as opposed to "COD" (Cash On Delivery)
    static final String SHIPMENT_TYPE = "NP";
    static final String STREET = "Unknown street";
    public static final String COUNTRY_CODE = "AT";
    static final String NAME = "Unknown recipient";


    private LabelDefaults() {
    }
}
