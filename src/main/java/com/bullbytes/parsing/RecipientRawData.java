package com.bullbytes.parsing;

import com.bullbytes.utils.Lists;
import net.jcip.annotations.Immutable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * {@link RecipientRawData} contains, for example, name, address, customer number of the person or company that
 * receives a parcel.
 * <p>
 * This data is contained line-by-line in the {@link RecipientRawData} and we don't know exactly which of
 * those lines denotes which of the recipient's data. That's why it's called <i>raw</i>. The
 * {@link com.bullbytes.parsing.parsers.DpdLabelParser} will take the {@link RecipientRawData} and create a proper
 * {@link com.bullbytes.dpdlabel.DpdLabel} from it.
 * <p>
 * Created by Matthias Braun on 23/08/16.
 */
@Immutable
public final class RecipientRawData {
    private static final Logger log = LoggerFactory.getLogger(RecipientRawData.class);
    private final List<String> lines;

    private RecipientRawData(List<String> lines) {
        this.lines = lines;
    }

    public static RecipientRawData createFrom(List<String> lines) {
        return new RecipientRawData(new ArrayList<>(lines));
    }

    @Override
    public String toString() {
        return "RecipientRawData{" + lines + '}';
    }

    public Optional<String> line(int index) {
        String line;
        if (index >= 0 && index < lines.size()) {
            line = lines.get(index);
        } else {
            line = null;
        }

        return Optional.ofNullable(line);
    }

    public List<String> getLines() {
        return new ArrayList<>(lines);
    }

    public RecipientRawData withoutFirstLine() {
        final RecipientRawData newRecipientData;
        if (lines.isEmpty()) {
            log.warn("Can't remove first line of recipient data since it is empty");
            newRecipientData = this;
        } else {
            newRecipientData = createFrom(lines.subList(1, lines.size()));
        }
        return newRecipientData;
    }

    public int getNrOfLines() {
        return lines.size();
    }

    public RecipientRawData withoutLine(int lineNr) {
        final RecipientRawData newData;
        if (lineNr >= 0 && lineNr < lines.size()) {
            lines.remove(lineNr);
            newData = createFrom(lines);

        } else {
            log.warn("Can't remove line at index {} because it is out of bounds for data: {}", lineNr, lines);
            newData = this;
        }
        return newData;
    }

    public Optional<String> lastLine() {
        return Lists.last(lines);
    }

}
