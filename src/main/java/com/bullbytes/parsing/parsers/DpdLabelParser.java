package com.bullbytes.parsing.parsers;

import com.bullbytes.dpdlabel.DpdLabel;
import com.bullbytes.dpdlabel.DpdLabel.Builder;
import com.bullbytes.dpdlabel.LabelDefaults;
import com.bullbytes.parsing.RecipientRawData;
import com.bullbytes.results.ParseResult;
import com.bullbytes.utils.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.*;

/**
 * Parses a {@link DpdLabel} from a MonKey Office delivery note PDF.
 * <p>
 * Created by Matthias Braun on 08/09/16.
 */
public final class DpdLabelParser {
    // This string precedes the customer number in the PDF's text. For example: "Kunden-Nr.: 1234567"
    static final String CUSTOMER_NR_PREFIX = "Kunden-Nr.:";
    // This string precedes the order number in the PDF's text. For example: "Bestell-Nr.: 1234567"
    private static final String ORDER_NR_PREFIX = "Bestell-Nr.:";
    private static final Logger log = LoggerFactory.getLogger(DpdLabelParser.class);
    // If we can't translate the country name in German to the ISO 3166 alpha-2 code, we return this as a fallback
    private static final String DEFAULT_RECIPIENT_COUNTRY_CODE = LabelDefaults.COUNTRY_CODE;

    private DpdLabelParser() {
    }

    /**
     * Attempts to parse a a {@link DpdLabel} from a MonKey Office {@code deliveryNotePdf}.
     *
     * @param deliveryNotePdf this file is treated as a PDF that contains a delivery note from MonKey Office. We try
     *                        to parse the recipient's data from this PDF and create a {@link DpdLabel} from it.
     * @return a {@link ParseResult} that contains the parsed {@link DpdLabel} if the parsing succeeded or the errors
     * that occurred if parsing failed
     */
    public static ParseResult parse(File deliveryNotePdf) {
        return PdfUtil.load(deliveryNotePdf)
                .flatMap(PdfUtil::getText)
                .map(RecipientRawDataParser::getRecipientData)
                .map(DpdLabelParser::parse).fold(
                        ParseResult::fromException,
                        ParseResult::fromLabel
                );
    }

    private static DpdLabel parse(RecipientRawData recipientData) {

        Builder label = new Builder();
        /*
          We expect the recipient data to look like this. Lines in parentheses may exist:

          1. (Salutation like "Herr" or "Frau" if the recipient is a private person)
          2. Recipient name, i.e., the name of a private person or a company
          3. (Academic title if the recipient is a private person or some custom text if the recipient is a company)
          4. (The person of contact in a company)
          5. Street with street number
          6. Postal code and city
          7. (Country name in German like "Österreich", "Deutschland")
          8. (Bestell-Nr.: the order number)
          9. Kunden-Nr.: the customer number

          First, we remove the salutation in the first line if it exists.
         */
        RecipientRawData dataWithoutSalutation = setSalutationThenRemoveLine(recipientData, label);

        /*
          Since we can easily check if there's an order number based on the known prefix "Bestell-Nr.:" we'll parse
          the order number and then remove that line from the recipient data if it exists.
         */
        RecipientRawData dataWithoutOrderNr = setOrderNrThenRemoveLine(dataWithoutSalutation, label);

        /*
          The last lines of the data look like this now:

          ...
          5. Street with street number
          6. Postal code and city
          7. (Country name like "Österreich", "Deutschland")
          8. Kunden-Nr.: the customer number

           Check if the next to last line is a country. If so, set it in the label and remove the line
         */

        RecipientRawData dataWithoutCountry = setCountryThenRemoveLine(dataWithoutOrderNr, label);
        /*
           Now we have:

           ...
           5. Street with street number
           6. Postal code and city
           7. Kunden-Nr.: the customer number

        We set the address and the customer number. Then we remove those lines.

        */
        RecipientRawData dataWithoutAddressAndCustomerNr = setAddressAndCustomerNrThenRemoveLines(dataWithoutCountry, label);

        /*
          This leaves us with these lines:

          1. Recipient name, i.e., the name of a private person or a company
          2. (Academic title if the recipient is a private person or some custom text if the recipient is a company)
          3. (The person of contact in a company)

          We get the recipient's name from the first line and remove it.
         */
        RecipientRawData rest = setRecipientNameThenRemoveLine(dataWithoutAddressAndCustomerNr, label);

        /*
          The rest of the recipient's data looks like this now:

          1. (Academic title if the recipient is a private person or some custom text if the recipient is a company)
          2. (The person of contact in a company)

          We'll try to figure out if any of those lines is an academic title by comparing it with a known set of titles.
          If it's a title we set it and remove the line.
         */
        RecipientRawData personOfContactMaybe = setAcademicTitleThenRemoveLine(rest, label);
        /*
           This is what we're left with:

          1. (Custom text for a company)
          2. (The person of contact in a company)

        So, all the possibilities are:

           1. Custom text for a company
           2. The person of contact in a company

           1. Custom text for a company

           1. The person of contact in a company

           (No line at all)

        It's a hard problem to tell if some string is a person's name so we just take the last line of the remaining
        recipient's data and set it as the person of contact. If we're lucky it is indeed the person of contact.
        Otherwise, we have set the custom text as the company's person of contact. This isn't so bad since when printing
        the label, the person of contact is printed beneath the company's name so the custom company text would appear there
        if we weren't lucky.
         */
        setLastLineAsPersonOfContact(personOfContactMaybe, label);

        return label.build();
    }

    private static RecipientRawData setCountryThenRemoveLine(RecipientRawData recipientData, Builder label) {
        // In the next to last line there is probably a country name in German. If not, we set the default country in the label
        int indexOfNextToLastLine = recipientData.getNrOfLines() - 2;
        return Lists.get(recipientData.getLines(), indexOfNextToLastLine)
                .map(nextToLastLine -> {
                    String trimmedLine = nextToLastLine.trim();
                    RecipientRawData newRecipientData;
                    String countryCode;
                    Optional<String> countryNameMaybe = getGermanCountryNameSimilarTo(trimmedLine);
                    if (countryNameMaybe.isPresent()) {
                        newRecipientData = recipientData.withoutLine(indexOfNextToLastLine);
                        countryCode = getCountryCodeOrWarn(countryNameMaybe.get());
                    } else {
                        // Some delivery notes don't contain a country: we assume a default country. It's important to
                        // not remove the next to last line so the rest of the recipient data gets parsed fine
                        newRecipientData = recipientData;
                        log.warn("There doesn't seem to be a country in this recipient's data. Using {} as fallback. Data: {}"
                                , LabelDefaults.COUNTRY_CODE, recipientData);
                        countryCode = LabelDefaults.COUNTRY_CODE;
                    }

                    label.setRecipientCountryCode(countryCode);
                    return newRecipientData;
                })
                .orElse(recipientData);
    }

    /**
     * Gets the German country name that is similar to the {@code text}
     *
     * @param text we get a German country name that is similar to this
     * @return a German country name that is similar to {@code text} or an empty {@link Optional} if the
     * {@code text} is not similar to any German country name
     */
    private static Optional<String> getGermanCountryNameSimilarTo(String text) {

        Set<String> countryNamesInGerman = Iso3166Alpha2.getGermanCountryNames();

        // The German country names in the file are capitalized: Österreich, Deutschland, etc -> Make sure
        // a string like österreich oder ÖSTERREICH is converted to the way the country name appears in the file
        String capitalizedText = Strings.capitalize(text.toLowerCase(Locale.ROOT));

        // We allow for typos in the country name by checking if the text is similar to any official country name
        return countryNamesInGerman.stream()
                .filter(countryName -> Strings.areSimilar(countryName, capitalizedText))
                .findFirst();
    }

    private static void setLastLineAsPersonOfContact(RecipientRawData recipientData, Builder label) {
        recipientData.lastLine().map(String::trim).ifPresent(label::setPersonOfContact);
    }

    private static boolean isAcademicTitle(String text) {
        return AcademicTitles.getKnownAbbreviatedTitles().stream()
                .anyMatch(title -> Strings.areSimilar(title, text));
    }

    private static RecipientRawData setAcademicTitleThenRemoveLine(RecipientRawData recipientData, Builder label) {
        // There might be a line consisting of an academic title in the recipient data
        return Lists.getFirstIndexWhere(recipientData.getLines(), line -> isAcademicTitle(line.trim()))
                .map(lineIndex -> {
                    String academicTitle = recipientData.getLines().get(lineIndex);
                    label.setAcademicTitle(academicTitle.trim());
                    return recipientData.withoutLine(lineIndex);
                }).orElse(recipientData);
    }

    private static RecipientRawData setRecipientNameThenRemoveLine(RecipientRawData recipientData, Builder label) {
        // We expect the recipient's name to be in the first line
        return recipientData.line(0)
                .map(firstLine -> {
                    label.setRecipientName(firstLine.trim());
                    return recipientData.withoutFirstLine();

                }).orElseGet(() -> {

                    log.warn("Can't get recipient's name since there is no recipient data");
                    return recipientData;
                });
    }

    private static List<String> getPrivatePersonSalutationsInGerman() {
        return Arrays.asList("Herr", "Frau");
    }

    private static boolean isPrivatePersonSalutation(String line) {
        String trimmedLine = line.trim();
        return getPrivatePersonSalutationsInGerman().stream()
                .anyMatch(trimmedLine::equalsIgnoreCase);
    }

    private static RecipientRawData setSalutationThenRemoveLine(RecipientRawData recipientData, Builder label) {
        return recipientData.line(0).map(firstLine -> {
            String trimmedLine = firstLine.trim();
            // Remove the first line if it's a salutation, otherwise leave the recipient data as it is
            if (isPrivatePersonSalutation(trimmedLine)) {
                label.setSalutation(trimmedLine);
                return recipientData.withoutFirstLine();
            } else {
                return recipientData;
            }
        }).orElse(recipientData);
    }

    private static RecipientRawData setAddressAndCustomerNrThenRemoveLines(RecipientRawData recipientData, Builder label) {

        final RecipientRawData dataWithoutLines;
        /*
           We expect this to be the last three lines of the passed recipient data:

           Street with street number
           Postal code and city
           Kunden-Nr.: the customer number

         */
        List<String> lines = recipientData.getLines();
        int startIndex = lines.size() - 3;
        int endIndex = lines.size() - 1;
        List<String> lastThreeLines = Lists.subList(lines, startIndex, endIndex);

        if (lastThreeLines.size() != 3) {
            log.warn("Could not get the last three lines from the recipient data: {}", recipientData);
            dataWithoutLines = recipientData;
        } else {
            String street = lastThreeLines.get(0).trim();

            String lineWithPostalCodeAndCity = lastThreeLines.get(1);

            String postalCode = getPostalCodeOrWarn(lineWithPostalCodeAndCity);
            String city = getCityOrWarn(lineWithPostalCodeAndCity);

            String customerNr = getCustomerNrOrWarn(lastThreeLines.get(2));

            label
                    .setRecipientStreet(street)
                    .setRecipientPostalCode(postalCode)
                    .setRecipientCity(city)
                    .setCustomerNr(customerNr);

            // Create new recipient data without the last three lines that we just parsed
            dataWithoutLines = RecipientRawData.createFrom(Lists.subList(lines, 0, startIndex - 1));
        }
        return dataWithoutLines;
    }

    private static String getCustomerNrOrWarn(String lineWithCustomerNr) {
        return getCustomerNr(lineWithCustomerNr)
                .orElseGet(() -> {
                    log.warn("Could not get customer number from this line, using '{}' as fallback: '{}'",
                            LabelDefaults.CUSTOMER_NR, lineWithCustomerNr);

                    return LabelDefaults.CUSTOMER_NR;
                });
    }

    private static String getCityOrWarn(String lineWithPostalCodeAndCity) {
        return getCity(lineWithPostalCodeAndCity)
                .orElseGet(() -> {
                    log.warn("Could not get city from this line, using '{}' as fallback: '{}'",
                            LabelDefaults.CITY, lineWithPostalCodeAndCity);

                    return LabelDefaults.CITY;
                });
    }

    private static String getPostalCodeOrWarn(String lineWithPostalCodeAndCity) {
        return getPostalCode(lineWithPostalCodeAndCity)
                .orElseGet(() -> {
                    log.warn("Could not get postal code from this line, using '{}' as fallback: '{}'",
                            LabelDefaults.POSTAL_CODE, lineWithPostalCodeAndCity);

                    return LabelDefaults.POSTAL_CODE;
                });
    }

    private static Optional<String> getPostalCode(String postalCodeAndCity) {
        // We get the string before the first space, so the postal code in "4502 St. Marien" is parsed correctly
        return Strings.getSubBeforeFirst(postalCodeAndCity.trim(), " ");
    }

    private static Optional<String> getCity(String postalCodeAndCity) {
        // We get the string after the first space, so the city in "4502 St. Marien" is parsed correctly
        return Strings.getSubAfterFirst(postalCodeAndCity.trim(), " ");
    }

    private static Optional<String> getCustomerNr(String line) {
        return Strings.getSubAfterLast(line, CUSTOMER_NR_PREFIX)
                .map(String::trim);
    }

    private static Optional<String> getOrderNumber(String line) {
        return Strings.getSubAfterLast(line, ORDER_NR_PREFIX)
                .map(String::trim);
    }

    private static String getCountryCodeOrWarn(String germanCountryName) {
        return Iso3166Alpha2.germanCountryNameToCode(germanCountryName)
                .orElseGet(() -> {
                    log.warn("Could not get ISO code for country '{}', using '{}' as fallback",
                            germanCountryName, DEFAULT_RECIPIENT_COUNTRY_CODE);
                    return DEFAULT_RECIPIENT_COUNTRY_CODE;
                });
    }

    private static Optional<Integer> getLineNrWithOrderNr(RecipientRawData data) {
        return Lists.getFirstIndexWhere(data.getLines(), line -> line.trim().startsWith(ORDER_NR_PREFIX));
    }

    private static RecipientRawData setOrderNrThenRemoveLine(RecipientRawData data, Builder label) {
        return getLineNrWithOrderNr(data)
                .flatMap(lineNr -> data.line(lineNr)
                        .flatMap(lineWithOrderNr -> getOrderNumber(lineWithOrderNr)
                                .map(orderNr -> {
                                    label.setOrderNr(orderNr);
                                    return data.withoutLine(lineNr);
                                })))
                .orElse(data);
    }
}
