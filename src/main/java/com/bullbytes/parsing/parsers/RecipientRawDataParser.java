package com.bullbytes.parsing.parsers;

import com.bullbytes.parsing.RecipientRawData;
import com.bullbytes.utils.Lists;
import com.google.common.base.Splitter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;


/**
 * Parses {@link RecipientRawData} from text that was taken from a MonKey Office delivery note.
 * <p>
 * Mind that this {@link RecipientRawDataParser} expects the delivery note to be from a MonKey Office where the
 * language is set to German.
 * <p>
 * Created by Matthias Braun on 07/09/16.
 */
final class RecipientRawDataParser {

    /**
     * After having converted the delivery note from PDF to text, recipient data starts after the line that starts with
     * this string if the MonKey Office user doesn't have a custom footer for their delivery notes (in that case, the
     * recipient data starts on the first line).
     * Note that on the PDF, this string actually occurs at the bottom of each page, designating the
     * telephone number of the company that creates the delivery note, not the recipient's telephone number.
     */
    private static final String RECIPIENT_DATA_START = "Telefon:";
    private static final String RECIPIENT_DATA_END = DpdLabelParser.CUSTOMER_NR_PREFIX;
    private static final Logger log = LoggerFactory.getLogger(RecipientRawDataParser.class);

    private RecipientRawDataParser() {
    }

    private static List<String> splitOnNewLine(CharSequence pdfText) {
        return Splitter.on("\n").splitToList(pdfText);
    }

    /**
     * Parses {@link RecipientRawData} from a MonKey Office delivery note in German.
     *
     * @param deliveryNotePdfText the delivery note is originally in PDF form. This is its string representation after
     *                            being turned to text using {@link com.bullbytes.utils.PdfUtil#getText}.
     * @return the {@link RecipientRawData} from the {@code deliveryNotePdfText}
     */
    static RecipientRawData getRecipientData(CharSequence deliveryNotePdfText) {
        log.debug("PDF as text:\n{}", deliveryNotePdfText);
        List<String> lines = splitOnNewLine(deliveryNotePdfText);
        /*
         If the MonKey Office user uses the default document footer, the recipient's contact data begins after the line
         that starts with "Telefon:" (which, on the PDF, is actually in the footer of the page).
         Otherwise the contact data starts the very first line, since the user has a custom footer such as an image.

         The recipient's contact data ends with the line that starts with "Kunden-Nr.:".
        */
        int endLineIndex = getEndLineIndex(lines);

        /* The text marking the beginning of the recipient data ("start marker") can occur later in the document,
         * after the recipient's data. This is the case when the MonKey Office user uses a custom footer that doesn't
         * contain the start marker. Obviously, we don't look for the start of the contact data after its end.
         */
        List<String> linesUntilEndOfRecipientData = Lists.subList(lines, 0, endLineIndex);
        int startLineIndex = getStartLineIndex(linesUntilEndOfRecipientData);

        List<String> recipientDataLines = Lists.subList(lines, startLineIndex, endLineIndex);
        RecipientRawData recipientData = RecipientRawData.createFrom(recipientDataLines);

        log.debug("Raw recipient data: {}", recipientData);
        return recipientData;
    }

    private static int getEndLineIndex(List<String> linesOfPdfText) {
        return Lists.getFirstIndexWhere(linesOfPdfText,
                line -> line.startsWith(RECIPIENT_DATA_END))
                .orElseGet(() -> {
                    log.warn("Does not contain the expected line that marks the end of recipient data ('{}'): {}",
                            RECIPIENT_DATA_END, linesOfPdfText);
                    // For the case that the list of lines is empty
                    return Math.max(0, linesOfPdfText.size() - 1);
                });
    }

    private static int getStartLineIndex(List<String> linesOfPdfText) {
        return Lists.getFirstIndexWhere(linesOfPdfText,
                line ->
                        line.startsWith(RECIPIENT_DATA_START))
                // The recipient data starts after this line
                .map(lineNr -> lineNr + 1)

                /* The MonKey Office user has a custom footer so the line for the default footer
                  which marks the beginning of the recipient's data doesn't occur in the text.
                  Instead the recipient's data starts at the first line of the text
                 */
                .orElse(0);
    }
}
