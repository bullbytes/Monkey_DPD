package com.bullbytes.results;

import net.jcip.annotations.Immutable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static java.util.stream.Collectors.joining;

/**
 * Tells us how updating a database went.
 * <p>
 * Created by Matthias Braun on 11/08/16.
 */
@Immutable
public final class DatabaseResult {
    private final String message;
    private final List<Exception> exceptions;
    private final ResultType resultType;

    private DatabaseResult(String message, ResultType type, Exception exception) {
        this.message = message;
        resultType = type;
        exceptions = Collections.singletonList(exception);
    }

    private DatabaseResult(String message, ResultType type, List<Exception> exceptions) {
        this.message = message;
        resultType = type;
        this.exceptions = exceptions;
    }

    /**
     * Creates a {@link DatabaseResult} from the numbers of rows that were updated in a database.
     *
     * @param rowCount this many rows were updated in a database
     * @return an initialized {@link DatabaseResult}
     */
    public static DatabaseResult fromRowCount(int rowCount) {
        String rowOrRows = rowCount == 1 ? "row" : "rows";
        return new DatabaseResult("Successfully updated " + rowCount + " " + rowOrRows,
                ResultType.SUCCESS,
                Collections.emptyList());
    }

    public static DatabaseResult fromException(Exception e) {
        String errorMsg = "Unable to send. Exception message: " + e.getMessage();
        return new DatabaseResult(errorMsg, ResultType.FAILURE, e);
    }

    String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        // Only print the exception if it exists
        String exceptionStr = exceptions.stream()
                .map(Throwable::toString)
                .collect(joining(", "));

        return "DatabaseResult{"
                + resultType + ", " +
                "message='" + message + '\'' + exceptionStr + "}";
    }

    List<Exception> getExceptions() {
        return new ArrayList<>(exceptions);
    }

    ResultType getType() {
        return resultType;
    }

}
