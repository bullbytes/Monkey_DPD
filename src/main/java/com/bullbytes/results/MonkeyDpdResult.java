package com.bullbytes.results;

import com.bullbytes.MonkeyDpd;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Describes the outcome of executing {@link MonkeyDpd}.
 * <p>
 * Created by Matthias Braun on 21/08/16.
 */
public final class MonkeyDpdResult {
    private final List<Exception> exceptions;
    private final String msg;
    private final ResultType type;

    private MonkeyDpdResult(ResultType type, String msg, List<Exception> exceptions) {
        this.type = type;
        this.msg = msg;
        this.exceptions = new ArrayList<>(exceptions);
    }

    public static MonkeyDpdResult fromException(String msg, Exception e) {
        return new MonkeyDpdResult(ResultType.FAILURE, msg, Collections.singletonList(e));
    }

    public static MonkeyDpdResult fromDatabaseResult(DatabaseResult databaseResult) {
        return new MonkeyDpdResult(databaseResult.getType(), databaseResult.getMessage(), databaseResult.getExceptions());
    }

    @Override
    public String toString() {
        return "MonkeyDpdResult{" +
                "type=" + type +
                ", msg='" + msg + '\'' +
                ", exceptions=" + exceptions +
                '}';
    }

    public boolean isSuccess() {
        return type == ResultType.SUCCESS;
    }
}
