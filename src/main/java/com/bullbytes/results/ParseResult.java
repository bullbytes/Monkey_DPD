package com.bullbytes.results;

import com.bullbytes.dpdlabel.DpdLabel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

/**
 * The outcome of parsing a MonKey Office delivery note.
 * <p>
 * Created by Matthias Braun on 21/08/16.
 */
public final class ParseResult {
    private static final Logger log = LoggerFactory.getLogger(ParseResult.class);
    private final List<DpdLabel> labels;
    private final Exception exception;
    private final ResultType type;

    private ParseResult(ResultType type, Exception exception, List<DpdLabel> parsedLabels) {
        this.type = type;
        this.exception = exception;
        labels = new ArrayList<>(parsedLabels);
    }

    public static ParseResult fromException(Exception ex) {
        return new ParseResult(ResultType.FAILURE, ex, Collections.emptyList());
    }

    public static ParseResult fromLabel(DpdLabel label) {
        return new ParseResult(ResultType.SUCCESS, null, Collections.singletonList(label));
    }

    /**
     * Applies the {@code labelMapper} if this {@link ParseResult} contains a success. Otherwise applies the
     * {@code exceptionMapper}
     *
     * @param exceptionMapper we apply this function from {@link Exception} to {@code R} if this {@link ParseResult}
     *                        contains a failure.
     * @param labelMapper     we apply this function from a list of {@link DpdLabel}s to {@code R} if this {@link ParseResult}
     *                        contains a success.
     * @param <R>             the result type of both the {@code exceptionMapper} and the {@code labelMapper}
     * @return the result of one of the passed function
     */
    public <R> R fold(Function<Exception, R> exceptionMapper, Function<List<DpdLabel>, R> labelMapper) {
        final R result;
        switch (type) {
            case SUCCESS:
                result = labelMapper.apply(labels);
                break;
            case FAILURE:
                result = exceptionMapper.apply(exception);
                break;
            default:
                log.warn("Unknown result type: {}. Using exception mapper", type);
                result = exceptionMapper.apply(exception);
                break;
        }
        return result;
    }

    public List<DpdLabel> getLabels() {
        return new ArrayList<>(labels);
    }

    public Optional<Exception> getException() {
        return Optional.ofNullable(exception);
    }
}
