package com.bullbytes.results;

/**
 * Whether {@link MonkeyDpdResult}, {@link ParseResult},
 * or {@link DatabaseResult} denote a positive or negative result.
 * <p>
 * Created by Matthias Braun on 21/08/16.
 */
public enum ResultType {
    SUCCESS, FAILURE
}
