package com.bullbytes.utils;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Helps with academic titles a person is awarded when finishing some degree.
 * <p>
 * Created by Matthias Braun on 30/08/16.
 */
public final class AcademicTitles {

    private AcademicTitles() {
    }

    /**
     * @return all academic titles that we know of in English and German.
     */
    public static List<String> getKnownAbbreviatedTitles() {

        List<String> allTitles = new ArrayList<>();
        allTitles.addAll(getAbbreviatedBachelorTitles());
        allTitles.addAll(getAbbreviatedMasterTitles());
        allTitles.addAll(getAbbreviatedPhDtitles());

        return allTitles;
    }

    private static List<String> getAbbreviatedPhDtitles() {
        return Arrays.asList("Dr","Dr.", "Dr. med. dent.", "Dr. med. univ.", "Dr. artium", "Dr. iur.",
                "Dr. med. dent. et scient. med.", "Dr. med. univ. et scient. med.", "Dr. med. vet.",
                "Dr. med. vet. et scient.", "Dr. mont.", "Dr. nat. techn.", "Dr. phil.", "Dr. phil. fac. theol.",
                "Dr. rer. cur.", "Dr. rer. nat.", "Dr. rer. oec.", "Dr. rer. soc. oec.",
                "Dr. scient. med.", "Dr. scient. vet.", "Dr. techn.", "Dr. theol.",
                "PhD", "Ph.D.", "Dr. phil.", "Dr. sc. hum.", "Dr. sc. inf. biomed.", "Dr. scient. pth."
        );
    }

    private static List<String> getAbbreviatedMasterTitles() {
        return Arrays.asList("LL.M.", "LLM. oec.", "LLM", "MA", "M.A.", "M.A.(Econ.)", "MArch", "M.Ed.Univ.",
                "MIBI", "MLBT", "M.phil.", "MSc", "M.Sc.", "MSc.", "MScMF", "MSSc", "MStat", "MTh",
                "M.Theol.", "MBA", "M.B.A.", "Dipl.-Ing.", "DI", "Dipl.-Ing. (FH)", "DI (FH)", "MLE", "E.MA",
                "EMLE", "EMPH", "MA Gastrosophy", "M.A.I.S.", "MBF", "M.B.L.", "MCF", "MDSc", "MED", "M.Ed.", "MEM",
                "M.Eng.", "M.E.S.", "MFA", "MFP", "MHE", "MHPE", "MIB", "MIM", "MLL", "MLS", "MoHE", "MPA", "MPH",
                "MPOS", "MPSt", "MSD", "MSPhT", "MTD", "MTox", "MMedScAA", "MMH", "PLL.M", "PMBA", "PMM", "PMML",
                "PM.ME", "PMPB", "PMPH", "PMSc", "MAS", "MDes", "MPC", "Mag","Mag.", "Mag. art.", "Mag. des. ind.", "Mag. (FH)",
                "Mag. iur.", "Mag. iur. rer. oec.", "Mag. med. vet.", "Mag. pharm.", "Mag. phil.", "Mag. Psych.",
                "Mag. pth.", "Mag. rel. paed.", "Mag. rer. nat.", "Mag. rer. soc. oec.", "Mag. sc. hum.", "Mag. theol.");
    }

    private static List<String> getAbbreviatedBachelorTitles() {
        return Arrays.asList("BA", "B.A.", "B.A.(Econ.)", "Bakk. art.", "Bakk. rer. nat.", "Bakk. phil.", "Bakk. iur.",
                "Bakk. techn.", "BArch", "BEd", "B.Ed.Univ.", "B.Eng.", "B.phil.", "B.Rel.Ed.Univ.", "BSc", "B.Sc.",
                "BScN", "BStat", "B.techn.", "BTh", "LL.B", "LLB. oec.", "LLB", "Bacc. rel. paed.",
                "Bakk.", "Bakk. pth.", "BBA", "B.B.A.", "BEng", "B.S.", "Bacc. theol.");
    }
}
