package com.bullbytes.utils;

import com.bullbytes.database.data.SqlRow;
import com.bullbytes.database.properties.DatabaseProperties;
import com.bullbytes.results.DatabaseResult;
import io.atlassian.fugue.Either;
import org.h2.jdbcx.JdbcDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

/**
 * Helps with databases such as getting data from them and storing data in them.
 * <p>
 * Created by Matthias Braun on 09/08/16.
 */
public final class DbUtil {

    // The database driver will replace this placeholder inside an SQL statement with escaped data.
    // This protects against SQL injection
    public static final String PLACEHOLDER = "?";
    private static final int QUERY_TIMEOUT_IN_SECONDS = 20;
    private static final Logger log = LoggerFactory.getLogger(DbUtil.class);

    private DbUtil() {
    }

    /**
     * Gets the location of a database from a {@code connection} as a URL.
     *
     * @param connection the {@link Connection} from which we want to get the database URL
     * @return {@link Optional optionally} the URL of the database, or an {@link Optional#empty()} if we couldn't get
     * the database URL
     */
    public static Optional<String> getUrl(Connection connection) {

        String url = null;
        if (connection != null) {
            try {
                DatabaseMetaData metaData = connection.getMetaData();
                if (metaData != null) {
                    url = metaData.getURL();
                } else {
                    log.warn("Meta data of connection {} is null", connection);
                }
            } catch (SQLException e) {
                log.warn("Could not get URL from connection", e);
            }
        } else {
            log.warn("Can't get database URL of null connection");
        }
        return Optional.ofNullable(url);
    }

    /**
     * Gets a {@link Connection} to an H2 database.
     *
     * @param dbProperties the {@link DatabaseProperties} used to establish the {@link Connection}. Contains the
     *                     credentials and the URL of the database
     * @return {@link Either either} a {@link Connection} to the H2 database or an {@link Exception} if we couldn't
     * establish the {@link Connection}
     */
    public static Either<Exception, Connection> getH2Connection(DatabaseProperties dbProperties) {
        Either<Exception, Connection> exceptionOrConnection;
        try {
            // In build.gradle we declare our dependency on H2, ensuring that this class is available
            Class.forName("org.h2.Driver");
            JdbcDataSource dataSource = new JdbcDataSource();
            dataSource.setURL(dbProperties.getUrl());
            dataSource.setUser(dbProperties.getUserName());
            dataSource.setPassword(dbProperties.getPassword());

            exceptionOrConnection = Either.right(dataSource.getConnection());
        } catch (ClassNotFoundException | SQLException e) {
            exceptionOrConnection = Either.left(e);
        }
        return exceptionOrConnection;
    }

    /**
     * Creates a {@link PreparedStatement} that inserts {@code rows} into a database {@code table}.
     *
     * @param table      we enter the {@code rows} into the database with this name
     * @param rows       these {@link SqlRow}s are inserted into the {@code table}. Mustn't be empty, otherwise we'll
     *                   return an {@link IllegalArgumentException}
     * @param connection the {@link Connection} used to create the {@link PreparedStatement} for inserting the {@code rows}
     * @return {@link Either either} a {@link PreparedStatement} that, when executed, insert the {@code rows} into
     * the table, or an {@link Exception} if there are no {@code rows} to insert or something went wrong while creating
     * the {@link PreparedStatement}
     */
    public static Either<Exception, PreparedStatement> createBatchInsert(String table,
                                                                         List<SqlRow> rows,
                                                                         Connection connection) {
        Either<Exception, PreparedStatement> exceptionOrStatement;
        if (!rows.isEmpty()) {

            // Assuming there are three values in a row, these placeholders for a prepared statement with two rows look
            // like this: (?, ?, ?), (?, ?, ?)
            String valuePlaceholders = rows.stream()
                    .map(SqlRow::getValuePlaceholders)
                    .collect(joining(", "));

            String sortedColumns = rows.get(0).getColumns().stream().collect(joining(", "));
            try {
                String sqlWithPlaceholders = "INSERT INTO " + table + " (" + sortedColumns + ") VALUES " + valuePlaceholders;
                PreparedStatement stmt = connection.prepareStatement(sqlWithPlaceholders);
                stmt.setQueryTimeout(QUERY_TIMEOUT_IN_SECONDS);

                // All the values of all the rows
                List<String> rowValues = rows.stream()
                        .map(SqlRow::getValues)
                        .flatMap(Collection::stream)
                        .collect(toList());

                // Replace the placeholders in the SQL statement with the values of each row
                Lists.forEachWithIndex(rowValues, (index, value) ->
                        // The value's index starts at one
                        setString(stmt, index + 1, value));

                /*
                  Using persons as an example type, the prepared statement evaluates to something like this at the
                  database if we want to insert two rows:
                  INSERT INTO peopleTable (firstName, lastName, age) VALUES("John", "Doe", 32), ("Jane", "Digger", 45);
                 */

                exceptionOrStatement = Either.right(stmt);

            } catch (SQLException e) {
                exceptionOrStatement = Either.left(e);
            }
        } else {
            exceptionOrStatement = Either.left(new IllegalArgumentException("There are no rows to insert"));
        }

        return exceptionOrStatement;
    }

    private static void setString(PreparedStatement stmt, int index, String value) {
        try {
            stmt.setString(index, value);
        } catch (SQLException e) {
            log.warn("Could not replace placeholder at index {} with value '{}'", index, value, e);
        }
    }

    /**
     * Executes an SQL {@code statement}, such as INSERT, UPDATE, DELETE
     *
     * @param statement we execute this {@link PreparedStatement}
     * @return a {@link DatabaseResult}
     */
    public static DatabaseResult executeUpdate(PreparedStatement statement) {
        DatabaseResult result;
        try {
            int rowCount = statement.executeUpdate();
            result = DatabaseResult.fromRowCount(rowCount);
        } catch (SQLException e) {
            result = DatabaseResult.fromException(e);
        }
        return result;
    }
}
