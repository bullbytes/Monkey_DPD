package com.bullbytes.utils;

import io.atlassian.fugue.Either;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * <p>
 * Created by Matthias Braun on 21/08/16.
 */
public final class EitherUtil {
    private EitherUtil() {
    }

    public static <L, R> List<L> toLeftList(Collection<Either<L, R>> eithers) {
        List<L> lefts = new ArrayList<>();
        eithers.forEach(either -> {
            if (either.isLeft()) {
                lefts.add(either.left().get());
            }
        });
        return lefts;
    }

    public static <L, R> List<R> toRightList(List<Either<L, R>> eithers) {

        List<R> rights = new ArrayList<>();
        eithers.forEach(either -> {
            if (either.isRight()) {
                rights.add(either.right().get());
            }
        });
        return rights;
    }
}
