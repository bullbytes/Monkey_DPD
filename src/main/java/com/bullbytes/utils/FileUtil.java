package com.bullbytes.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

/**
 * Helps with {@link File}s.
 * <p>
 * Created by Matthias Braun on 18/08/16.
 */
public final class FileUtil {

    private static final Logger log = LoggerFactory.getLogger(FileUtil.class);

    private FileUtil() {
    }

    /**
     * Deletes a file if it exists.
     * Logs warning if it existed but could not be deleted.
     *
     * @param file we want to delete this file
     */
    public static boolean delete(File file) {

        boolean success = false;
        try {
            success = Files.deleteIfExists(file.toPath());
        } catch (IOException e) {
            log.warn("Could not delete file at '{}'", file, e);
        }
        return success;
    }
}
