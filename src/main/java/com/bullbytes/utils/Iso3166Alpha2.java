package com.bullbytes.utils;

import com.google.common.base.Splitter;
import com.google.common.io.Resources;
import org.apache.pdfbox.util.Charsets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

/**
 * Provides standardized country codes according to ISO 3166 Alpha of the International Organization for
 * Standardization (ISO).
 * <p>
 * Created by Matthias Braun on 13/08/16.
 */
public final class Iso3166Alpha2 {
    private static final String ISO_3166_ALPHA2_GERMAN_FILE = "iso_3166_alpha2_german.properties";
    private static final Logger log = LoggerFactory.getLogger(Iso3166Alpha2.class);

    private Iso3166Alpha2() {
    }

    /**
     * @return a {@link Map} from ISO 3166 alpha 2 country codes to their respective country names in German.
     * If the file in the resource folder containing the country code data could not be loaded, we return an empty
     * {@link Map}
     * @see <a href="https://de.wikipedia.org/wiki/ISO-3166-1-Kodierliste">ISO 3166</a>
     */
    private static Map<String, List<String>> getCodesAndGermanCountries() {
        return loadResource(ISO_3166_ALPHA2_GERMAN_FILE)
                .map(Iso3166Alpha2::getCodesAndGermanCountries)
                .orElse(new HashMap<>());
    }

    /**
     * @param countryFile the {@link URL} to the file containing the ISO country codes and their associated
     *                    country names in German.
     *                    We expect the codes and the country names to be separated by equal signs.
     *                    We also expect that the individual country names are separated by semicolons.
     * @return a {@link Map} from ISO 3166 alpha 2 country codes to their respective country names in German
     * @see <a href="https://de.wikipedia.org/wiki/ISO-3166-1-Kodierliste">ISO 3166</a>
     */
    private static Map<String, List<String>> getCodesAndGermanCountries(URL countryFile) {
        Map<String, List<String>> isoCodesAndCountries = new HashMap<>();
        try {
            List<String> lines = Resources.readLines(countryFile, Charsets.UTF_8);
            lines.forEach(line -> {
                String separator = "=";
                int splitAt = line.indexOf(separator);
                if (splitAt != -1) {
                    String isoCode = line.substring(0, splitAt).trim();
                    String countries = line.substring(splitAt + separator.length(), line.length()).trim();
                    List<String> countryList = Splitter.on(";").splitToList(countries)
                            .stream()
                            .map(String::trim)
                            .collect(toList());

                    isoCodesAndCountries.put(isoCode, countryList);
                }
            });

        } catch (IOException e) {
            log.warn("Could not read {}", countryFile, e);
        }
        return isoCodesAndCountries;
    }

    private static Optional<URL> loadResource(String fileName) {
        URL url = null;
        try {
            url = Resources.getResource(fileName);

        } catch (IllegalArgumentException e) {
            log.warn("Could not find resource at {}", fileName, e);

        }
        return Optional.ofNullable(url);
    }

    public static Optional<String> germanCountryNameToCode(String germanCountryName) {
        return getCodesAndGermanCountries().entrySet().stream()
                .filter(entry -> entry.getValue().contains(germanCountryName))
                .findFirst()
                .map(Entry::getKey);
    }

    public static Set<String> getGermanCountryNames() {
        return getCodesAndGermanCountries().values().stream()
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());
    }
}
