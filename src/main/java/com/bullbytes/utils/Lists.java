package com.bullbytes.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.List;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;

/**
 * Helps with {@link List}s.
 * <p>
 * Created by Matthias Braun on 12/08/16.
 */
public final class Lists {

    public static final int NOT_FOUND_INDEX = -1;
    private static final Logger log = LoggerFactory.getLogger(Lists.class);

    private Lists() {
    }

    public static List<String> getDuplicates(List<String> list) {
        return list.stream()
                .collect(Collectors.groupingBy(elem -> elem))
                .entrySet().stream()
                .filter(entry -> entry.getValue().size() > 1)
                .map(Entry::getKey)
                .collect(toList());
    }

    public static <E> void forEachWithIndex(List<E> list, BiConsumer<Integer, E> consumer) {
        for (int i = 0; i < list.size(); i++) {
            E element = list.get(i);
            consumer.accept(i, element);
        }
    }

    public static <E> Optional<E> getFirstElemWhere(List<E> list, Predicate<E> predicate) {
        return getFirstIndexWhere(list, predicate).flatMap(index -> get(list, index));
    }

    public static <E> Optional<Integer> getFirstIndexWhere(List<E> list, Predicate<E> predicate) {
        Integer index = null;
        for (int i = 0; i < list.size(); i++) {
            E element = list.get(i);
            if (predicate.test(element)) {
                index = i;
                break;
            }
        }
        return Optional.ofNullable(index);
    }

    public static <E> Optional<E> get(List<E> list, int index) {
        final E element;
        if (index >= 0 && index < list.size()) {
            element = list.get(index);
        } else {
            element = null;
        }
        return Optional.ofNullable(element);
    }

    public static <Start, End> List<End> mapToList(Collection<Start> list, Function<Start, End> mapper) {
        return list.stream().map(mapper).collect(toList());
    }

    /**
     * Gets a sub list from a {@code list}.
     * <p>
     * This method differs in two substantial ways from {@link List#subList}:
     * <ol>
     * <li> This method's end index is inclusive </li>
     * <li> This method is more lenient: It won't throw an {@link IndexOutOfBoundsException} when the indices are out
     * of bounds or an {@link IllegalArgumentException} when the start index comes after the end index but rather
     * warn and return the empty list</li>
     * </ol>
     *
     * @param list           we get a sub list from this {@link List}
     * @param startInclusive the sub list starts here. The line with this index in the {@code list} is the first line in the returned list
     * @param endInclusive   the sub list ends here. The line with this index in the {@code list} is the last line in the returned list
     * @param <E>            the elements inside the {@code list}
     * @return a sub list from {@code starInclusive} to {@code endInclusive} of the {@code list}
     */
    public static <E> List<E> subList(List<E> list, int startInclusive, int endInclusive) {
        final List<E> subList;
        if (list == null) {
            log.warn("Can't get sub list from null. Returning empty list");
            subList = emptyList();
        } else if (startInclusive < 0 || startInclusive >= list.size()) {
            log.warn("Start index is out of range ({}) of list with size {}, returning empty list",
                    startInclusive, list.size());
            subList = emptyList();
        } else if (endInclusive < 0 || endInclusive >= list.size()) {
            log.warn("End index is out of range ({}) of list with size {}, returning empty list",
                    endInclusive, list.size());
            subList = emptyList();

        } else if (startInclusive > endInclusive) {
            log.warn("Start of sub list {} can't come after end {}, returning empty list ",
                    startInclusive, endInclusive);
            subList = emptyList();
        } else {
            subList = list.subList(startInclusive, endInclusive + 1);
        }

        return subList;
    }

    public static Optional<String> last(List<String> lines) {
        return lines.isEmpty() ?
                Optional.empty() :
                Optional.ofNullable(lines.get(lines.size() - 1));
    }
}
