package com.bullbytes.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Helps with {@link Map}s.
 * <p>
 * Created by Matthias Braun on 12/08/16.
 */
public final class MapUtil {
    private MapUtil() {
    }

    public static <K, V> List<V> getSortedValues(Map<K, V> map, List<K> sortedKeys) {
        List<V> sortedValues = new ArrayList<V>();
        sortedKeys.forEach(key -> sortedValues.add(map.get(key)));
        return sortedValues;
    }

    public static <K, V> Map<String, V> keysToString(Map<K, V> map) {
        Map<String, V> mapWithStringKeys = new HashMap<>();
        map.forEach((key, value) -> mapWithStringKeys.put(String.valueOf(key), value));
        return mapWithStringKeys;
    }
}
