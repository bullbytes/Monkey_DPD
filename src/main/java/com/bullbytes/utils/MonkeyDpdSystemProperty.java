package com.bullbytes.utils;

import java.util.Optional;

/**
 * System properties that this program uses. Callers of this program can set these properties by calling this program
 * using {@code java -Dthe.property=value thisJar}
 * <p>
 * Created by Matthias Braun on 16/08/16.
 */
public enum MonkeyDpdSystemProperty {

    /**
     * We expect that this system property has the value of the path to the configuration file (a .properties file)
     */
    CONFIG_FILE("monkeyDpd.config");

    private final String propertyAsString;

    MonkeyDpdSystemProperty(String propertyAsString) {
        this.propertyAsString = propertyAsString;
    }

    public static Optional<String> get(MonkeyDpdSystemProperty property) {
        return Optional.ofNullable(System.getProperty(property.toString()));
    }

    /**
     * @return the name of the property in the {@link #CONFIG_FILE} that tells us to which file to log
     */
    public static String getLogFilePropertyName() {
        return "log.file";
    }

    @Override
    public String toString() {
        return propertyAsString;
    }
}
