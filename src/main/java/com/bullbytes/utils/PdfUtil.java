package com.bullbytes.utils;

import io.atlassian.fugue.Either;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;

/**
 * Helps dealing with the Portable Document Format.
 * <p>
 * Created by admin on 06/08/16.
 */
public final class PdfUtil {

    private static final Logger log = LoggerFactory.getLogger(PdfUtil.class);

    private PdfUtil() {
    }

    /**
     * Gets the text from a PDF {@code document}
     *
     * @param document we get the text from this {@link PDDocument}
     * @return {@link Either either} the text of the {@code document}, or an {@link Exception} if we failed to get the
     * text
     */
    public static Either<Exception, String> getText(PDDocument document) {
        Either<Exception, String> exceptionOrText;
        try {
            PDFTextStripper stripper = new PDFTextStripper();
            exceptionOrText = Either.right(stripper.getText(document));

        } catch (IOException e) {
            exceptionOrText = Either.left(e);
        } finally {
            closePdf(document);
        }
        return exceptionOrText;
    }

    /**
     * Closes a {@link PDDocument}.
     *
     * @param document the {@link PDDocument} we want to close
     */
    private static void closePdf(Closeable document) {
        if (document == null) {
            log.warn("Can't close null object");
        } else {
            try {
                document.close();
            } catch (final IOException e) {
                log.warn("Could not close '{}'", document, e);
            }
        }
    }

    /**
     * Opens a {@code file} as a {@link PDDocument}.
     *
     * @param file we want to open this {@link File} and use it as a {@link PDDocument}
     * @return {@link Either either} the {@code file} as a {@link PDDocument}, or an {@link Exception} if we couldn't
     * load the {@code file}
     */
    public static Either<Exception, PDDocument> load(File file) {

        Either<Exception, PDDocument> exceptionOrPdf;
        try {
            exceptionOrPdf = Either.right(PDDocument.load(file));
        } catch (IOException e) {
            exceptionOrPdf = Either.left(e);
        }
        return exceptionOrPdf;
    }
}
