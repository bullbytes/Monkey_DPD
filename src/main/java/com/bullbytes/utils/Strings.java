package com.bullbytes.utils;

import info.debatty.java.stringsimilarity.JaroWinkler;
import info.debatty.java.stringsimilarity.interfaces.StringSimilarity;

import java.util.Collection;
import java.util.Locale;
import java.util.Optional;

import static com.bullbytes.utils.Lists.NOT_FOUND_INDEX;

/**
 * Helps with strings of characters.
 * <p>
 * Created by Matthias Braun on 11/08/16.
 */
public final class Strings {

    /**
     * We use the Jaro-Winkler algorithm to determine if two strings are similar. Jaro-Winkler detects high similarity
     * in strings when one of them was misspelled. https://github.com/tdebatty/java-string-similarity#jaro-winkler
     */
    private static final StringSimilarity jaroWinkler = new JaroWinkler();
    // Two strings are considered similar if their Jaro-Winkler similarity is above this value
    private static final double SIMILARITY_THRESHOLD = 0.85;

    private Strings() {
    }

    /**
     * Repeats a {@code stringToRepeat} a couple of {@code times}, placing a {@code delimiter} in between.
     *
     * @param stringToRepeat we repeat this {@link String}
     * @param times          we repeat the {@code stringToRepeat} this many times
     * @param delimiter      we place this {@link String} between the repetitions of the {@code stringToRepeat}
     * @return a new string that consists of the {@code stringToRepeat} repeated a couple of {@code times}
     */
    public static String repeat(String stringToRepeat, int times, String delimiter) {
        int lengthOfResult = (stringToRepeat.length() + delimiter.length()) * times;
        StringBuilder builder = new StringBuilder(lengthOfResult);
        for (int i = 0; i < times; i++) {
            builder.append(stringToRepeat);
            builder.append(delimiter);
        }
        // Remove the last delimiter
        replaceLast(builder, delimiter, "");
        return builder.toString();
    }

    /**
     * Gets the string after the last occurrence of  {@code afterThis} in the {@code original}.
     *
     * @param original  we'll look for {@code afterThis} in this {@link String}. If we find it, we return the
     *                  {@link String} after its last occurrence
     * @param afterThis we'll look for this {@link String} in {@code original}. If we find it, we return the
     *                  {@link String} after its last occurrence
     * @return {@link Optional optionally}, the string {@code afterThis} in the {@code original}, or
     * {@link Optional#empty()} if the {@code original} doesn't contain {@code afterThis}
     */
    public static Optional<String> getSubAfterLast(String original, String afterThis) {
        int index = original.lastIndexOf(afterThis);
        String sub = (index == NOT_FOUND_INDEX) ?
                null :
                original.substring(index + afterThis.length(), original.length());
        return Optional.ofNullable(sub);
    }

    /**
     * Gets the string before {@code beforeThis} in the {@code original}.
     *
     * @param original   we'll look for {@code beforeThis} in this {@link String}. If we find it, we return the
     *                   {@link String} before its first occurrence
     * @param beforeThis we'll look for this {@link String} in {@code original}. If we find it, we return the
     *                   {@link String} before its first occurrence
     * @return {@link Optional optionally}, the string {@code beforeThis} in the {@code original}, or
     * {@link Optional#empty()} if the {@code original} doesn't contain {@code beforeThis}
     */
    public static Optional<String> getSubBeforeFirst(String original, String beforeThis) {
        int index = original.indexOf(beforeThis);
        String sub = index == NOT_FOUND_INDEX ?
                null :
                original.substring(0, index);

        return Optional.ofNullable(sub);
    }

    /**
     * Gets the string after the first occurrence of  {@code afterThis} in the {@code original}.
     *
     * @param original  we'll look for {@code afterThis} in this {@link String}. If we find it, we return the
     *                  {@link String} after its first occurrence
     * @param afterThis we'll look for this {@link String} in {@code original}. If we find it, we return the
     *                  {@link String} after its first occurrence
     * @return {@link Optional optionally}, the string {@code afterThis} in the {@code original}, or
     * {@link Optional#empty()} if the {@code original} doesn't contain {@code afterThis}
     */
    public static Optional<String> getSubAfterFirst(String original, String afterThis) {

        int index = original.indexOf(afterThis);
        String sub = index == NOT_FOUND_INDEX ?
                null :
                original.substring(index + afterThis.length(), original.length());

        return Optional.ofNullable(sub);
    }

    /**
     * Determines if two strings are similar to each other. We use the {@link JaroWinkler Jaro-Winkler} algorithm
     * to calculate similarity. This means, for example, that two strings where one is the correct spelling and the other
     * is misspelled have a very high similarity.
     *
     * @param string1 we determine if this {@link String} is similar to {@code string2}
     * @param string2 we determine if this {@link String} is similar to {@code string1}
     * @return whether the two {@link String}s are similar to each other
     */
    public static boolean areSimilar(String string1, String string2) {

        double similarity = jaroWinkler.similarity(string1, string2);
        return similarity > SIMILARITY_THRESHOLD;
    }

    /**
     * Determines if a {@code string} is among a {@code list}, ignoring case.
     *
     * @param list           we check if the {@code string} is among these {@link String}s, ignoring case
     * @param maybeContained we check if this {@link String} is among the {@code list}, ignoring case
     * @return whether the {@code string} is among the {@code list}, ignoring case
     */
    public static boolean containsIgnoreCase(Collection<String> list, String maybeContained) {
        return list.stream().anyMatch(listElem -> listElem.equalsIgnoreCase(maybeContained));
    }

    /**
     * Capitalizes a string: java -> Java
     *
     * @param string we capitalize this string
     * @return the capitalized string or the unchanged string if it's null or is empty
     */
    public static String capitalize(String string) {
        String capitalizedString;
        if (string != null && !string.isEmpty()) {
            capitalizedString = string.substring(0, 1).toUpperCase(Locale.ROOT) + string.substring(1);
        } else {
            capitalizedString = string;
        }
        return capitalizedString;
    }

    /**
     * Replaces the last occurrence of {@code replaceThis} inside the {@code original} {@link StringBuilder}
     * {@code withThat}.
     * <p>
     * If the {@code original} doesn't contain {@code replaceThis}, we don't change {@code original}.
     *
     * @param original    this is the {@link StringBuilder} where we'll replace the last occurrence of {@code replaceThis}
     *                    {@code withThat}
     * @param replaceThis we look for this {@link String} inside {@code original}. If we find it, we'll replace its last
     *                    occurrence {@code withThat}
     * @param withThat    this is the {@link String} that replaces the last occurrence of {@code replaceThis} in
     *                    {@code original}
     */
    private static void replaceLast(StringBuilder original, String replaceThis, String withThat) {
        int index = original.lastIndexOf(replaceThis);
        if (index != NOT_FOUND_INDEX) {
            original.replace(index, index + replaceThis.length(), withThat);
        }
    }
}
