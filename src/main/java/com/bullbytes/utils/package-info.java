/**
 * Contains utility classes that are independent of the application's domain
 * <p>
 * Created by Matthias Braun on 13/08/16.
 */
package com.bullbytes.utils;