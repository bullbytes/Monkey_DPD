import com.bullbytes.utils.MonkeyDpdSystemProperty

import java.nio.file.Paths

// Info on patterns: http://logback.qos.ch/manual/layouts.html#conversionWord
def location = "%class.%method\\(%file:%line\\)"
def logMsgPattern = "%level: '%msg' %d{yyyy-MMM-dd HH:mm:ss} from $location%n"

def consoleAppenderName = 'console logger'
def fileAppenderName = 'file logger'


appender(consoleAppenderName, ConsoleAppender) {
  encoder(PatternLayoutEncoder) {
    pattern = logMsgPattern
  }
}

appender(fileAppenderName, FileAppender) {
  file = getLogFile()
  encoder(PatternLayoutEncoder) {
    pattern = logMsgPattern;
  }
}

root(TRACE, [consoleAppenderName, fileAppenderName])

/**
 * Gets the file we will log to. We expect that the path to this file is in a .properties file that is passed
 * to this program using a system property
 * @see MonkeyDpdSystemProperty
 * @return the file we will log to
 */
def getLogFile() {

  Properties properties = new Properties()
  def configFile = getConfigFileFromSystemProperty()
  File propertiesFile = new File(configFile)

  try {
    propertiesFile.withInputStream {
      properties.load(it)
    }
  } catch (IOException e) {
    System.err.println "Couldn't read file '$configFile'. Exception: $e"
  }

  /*
   * We expect that this is the variable name for the file we log to in the
   * config file passed to this program using the system property defined in {@link MonkeyDpdSystemProperty}.
   */
  def logFilePropertyName = MonkeyDpdSystemProperty.getLogFilePropertyName()
  def logFile = properties."$logFilePropertyName"
  if (logFile == null) {
    logFile = getDefaultLogFile();
  }
  logFile
}

def getDefaultLogFile() {
  String logDir;
  String userHome = System.getProperty("user.home")
  if (userHome == null) {
    System.err.println "User home directory is null. Getting current directory"
    String userDir = System.getProperty("user.dir")
    if (userDir == null) {
      String fallbackDir = ".";
      System.err.println "Current user directory is null. Using '$fallbackDir' as fallback"
      logDir = fallbackDir;
    } else {
      logDir = userDir;
    }
  } else {
    logDir = userHome;
  }

  def defaultLogFileName = "monkeyDpd.log";
  String logFile = Paths.get(logDir, defaultLogFileName).toString();
  println "Logging to '$logFile' as fallback"
  logFile;
}

def getConfigFileFromSystemProperty() {
  def configFileSystemProp = MonkeyDpdSystemProperty.CONFIG_FILE
  MonkeyDpdSystemProperty.get(configFileSystemProp)
          .orElse("no config file passed using system property '$configFileSystemProp'")
}
