package com.bullbytes;

import com.bullbytes.dpdlabel.DpdLabel;
import com.bullbytes.results.ParseResult;
import com.bullbytes.parsing.deliverynotes.DeliveryNotesForPrivatePerson;
import com.bullbytes.parsing.parsers.DpdLabelParser;
import com.google.common.io.Files;
import io.atlassian.fugue.Either;
import org.apache.pdfbox.util.Charsets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.junit.Assert.assertEquals;

/**
 * Utility methods for performing unit tests.
 * <p>
 * Created by Matthias Braun on 19/08/16.
 */
public final class TestUtil {
    private static final Logger log = LoggerFactory.getLogger(TestUtil.class);

    private TestUtil() {
    }

    /**
     * Loads a resource, such as a file from the resources folder.
     *
     * @param fileName the name of the resource we want to load
     * @return the loaded resource as a {@link File}
     */
    public static Either<Exception, File> loadResource(String fileName) {
        Either<Exception, File> exceptionOrFile;
        URL url = TestUtil.class.getResource(fileName);
        try {

            if (url != null) {
                // We convert the URL to an URI so loading of files with spaces in their name works
                URI uri = url.toURI();
                exceptionOrFile = Either.right(new File(uri));
            } else {
                String errorMsg = "No resource found at '" + fileName + "'";
                exceptionOrFile = Either.left(new FileNotFoundException(errorMsg));
            }
        } catch (URISyntaxException e) {
            log.warn("Could not load resource '{}'", fileName, e);
            exceptionOrFile = Either.left(e);
        }
        return exceptionOrFile;
    }

    /**
     * Loads a resource, such as a file from the resources folder.
     * <p>
     * If we can't load the resource, the returned {@link File} contains an error message as its path. If you want
     * to make the failed loading explicit using the type system, try {@link #loadResource(String)}.
     *
     * @param fileName the name of the resource we want to load
     * @return the loaded resource as a {@link File}
     */
    public static File load(String fileName) {
        return loadResource(fileName)
                .rightOr(e -> new File(getCouldntLoadMessage(fileName, e)));
    }

    public static DpdLabel parseSingleLabel(File file) {
        ParseResult parseResult = DpdLabelParser.parse(file);
        List<DpdLabel> labels = parseResult.getLabels();
        assertEquals("There should be data for a single DPD label in '" + file + "'. Exception while parsing: " + parseResult.getException(), 1, labels.size());
        return labels.get(0);
    }

    public static String getParseError(File file) {
        return "Incorrect output after parsing '" + file + "'";
    }

    public static List<File> getFilesInDirectory(String directory) {
        return getResourceFiles(directory).stream()
                // Use forward slashes as path separators, also on Windows. Otherwise the file can't be loaded
                .map(fileName -> directory + "/" + fileName)
                .map(TestUtil::load)
                .collect(toList());
    }

    /**
     * Writes all the fields into different files. One file per data field (e.g., the recipient's name).
     * Used for sanity checking the parse results by a human.
     *
     * @param label this is the parsed {@link DpdLabel}
     * @param dir   we'll write the different files in this directory.
     */
    public static void writeToFiles(DpdLabel label, String dir) {

        File nameFile = new File(dir + "names.txt");
        File cityFile = new File(dir + "cities.txt");
        File postalCodeFile = new File(dir + "postal_codes.txt");
        File customerNrFile = new File(dir + "customer_nrs.txt");
        File countryFile = new File(dir + "countries.txt");
        File streetFile = new File(dir + "streets.txt");
        File orderNrFile = new File(dir + "order_nrs.txt");
        File titleFile = new File(dir + "academic_titles.txt");
        File personOfContactFile = new File(dir + "person_of_contacts.txt");

        String recipientName = label.getRecipientName();


        String city = label.getRecipientCity();
        String postalCode = label.getRecipientPostalCode();
        String customerNr = label.getCustomerNr();
        String countryCode = label.getRecipientCountryCode();
        String street = label.getRecipientStreet();

        appendWithLineBreak(recipientName, nameFile);
        appendWithLineBreak(city, cityFile);
        appendWithLineBreak(postalCode, postalCodeFile);
        appendWithLineBreak(customerNr, customerNrFile);
        appendWithLineBreak(countryCode, countryFile);
        appendWithLineBreak(street, streetFile);

        label.getOrderNr().ifPresent(orderNr -> appendWithLineBreak(orderNr, orderNrFile));

        label.getAcademicTitle().ifPresent(title -> appendWithLineBreak(title, titleFile));
        label.getPersonOfContact().ifPresent(personOfContact -> appendWithLineBreak(personOfContact, personOfContactFile));

    }

    private static String getCouldntLoadMessage(String fileName, Exception e) {
        return "Couldn't load '" + fileName + "'. Exception: " + e;
    }

    /**
     * Gets all the file names inside a {@code directory} of our resources.
     *
     * @param directory the path to the directory inside our resources in this application's jar file
     * @return a list of file names inside the {@code directory}
     */
    private static List<String> getResourceFiles(String directory) {
        List<String> fileNames = new ArrayList<>();

        try (InputStream stream = getResourceAsStream(directory);
             BufferedReader reader = new BufferedReader(new InputStreamReader(stream, StandardCharsets.UTF_8))) {

            String resource;
            while ((resource = reader.readLine()) != null) {
                fileNames.add(resource);
            }
        } catch (IOException e) {
            log.warn("Could not list resource files", e);
        }

        return fileNames;
    }

    private static InputStream getResourceAsStream(String resource) {
        final InputStream stream = getContextClassLoader().getResourceAsStream(resource);

        return stream == null ? DeliveryNotesForPrivatePerson.class.getResourceAsStream(resource) : stream;
    }

    private static ClassLoader getContextClassLoader() {
        return Thread.currentThread().getContextClassLoader();
    }

    private static void appendWithLineBreak(String text, File file) {
        try {
            Files.append(text + System.lineSeparator(), file, Charsets.UTF_8);
        } catch (IOException e) {
            log.warn("Could not write {} to {}", text, file, e);
        }
    }
}
