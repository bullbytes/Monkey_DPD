package com.bullbytes.parsing;

import com.bullbytes.TestUtil;
import com.bullbytes.parsing.parsers.DpdLabelParser;
import org.junit.Test;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static com.bullbytes.TestUtil.getParseError;
import static com.bullbytes.TestUtil.parseSingleLabel;
import static com.github.npathai.hamcrestopt.OptionalMatchers.hasValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * Tests whether the {@link DpdLabelParser} can parse a delivery note for a company
 * (as opposed to a {@link DpdLabelParserTestForPrivatePerson private person}).
 * <p>
 * Created by Matthias Braun on 19/08/16.
 */
public final class DpdLabelParserTestForCompany {

    private static File getDeliveryNoteWithoutOrderNr() {
        String fileName = "/delivery notes/for companies/without order number.pdf";
        return TestUtil.load(fileName);
    }

    private static File getDeliveryNoteWithOrderNr() {
        String fileName = "/delivery notes/for companies/with order number.pdf";
        return TestUtil.load(fileName);
    }

    private static List<File> getDeliveryNotesWithOrderNumber() {
        return Arrays.asList(
                getDeliveryNoteWithoutPersonOfContact(),
                getDeliveryNoteWithOrderNr());
    }

    private static File getDeliveryNoteWithoutPersonOfContact() {
        String fileName = "/delivery notes/for companies/no person of contact.pdf";
        return TestUtil.load(fileName);
    }

    private static List<File> getDeliveryNotesWithPersonOfContact() {
        return Arrays.asList(
                getDeliveryNoteWithoutOrderNr(),
                getDeliveryNoteWithOrderNr());
    }

    private static List<File> getDeliveryNotes() {
        return Arrays.asList(
                getDeliveryNoteWithoutOrderNr(),
                getDeliveryNoteWithoutPersonOfContact(),
                getDeliveryNoteWithOrderNr());
    }

    @Test
    public void parseOrderNumber() {
        String expectedOrderNr = "#1234567";

        getDeliveryNotesWithOrderNumber().forEach(file -> {
            Optional<String> parsedOrderNr = parseSingleLabel(file).getOrderNr();

            assertThat(parsedOrderNr, hasValue(expectedOrderNr));
        });
    }

    @Test
    public void orderNrIsEmptyWhenParsingNoteWithoutOrderNr() {

        File deliveryNote = getDeliveryNoteWithoutOrderNr();
        Optional<String> parsedOrderNr = parseSingleLabel(deliveryNote).getOrderNr();

        assertEquals(getParseError(deliveryNote), Optional.empty(), parsedOrderNr);
    }

    @Test
    public void personOfContactIsEmptyWhenParsingNoteWithoutPersonOfContact() {

        File deliveryNote = getDeliveryNoteWithoutPersonOfContact();
        Optional<String> parsedPersonOfContact = parseSingleLabel(deliveryNote).getPersonOfContact();

        assertEquals(getParseError(deliveryNote), Optional.empty(), parsedPersonOfContact);
    }

    @Test
    public void parseCustomerNumber() {
        String customerNr = "ADR-000003";

        getDeliveryNotes().forEach(file -> {
            String parsedCustomerNr = parseSingleLabel(file).getCustomerNr();

            assertEquals(getParseError(file), customerNr, parsedCustomerNr);
        });
    }

    @Test
    public void parsePostalCode() {
        String recipientPostalCode = "4020";

        getDeliveryNotes().forEach(file -> {
            String parsedCity = parseSingleLabel(file).getRecipientPostalCode();

            assertEquals(getParseError(file), recipientPostalCode, parsedCity);
        });
    }

    @Test
    public void parseCity() {

        String recipientCity = "Linz";

        getDeliveryNotes().forEach(file -> {
            String parsedCity = parseSingleLabel(file).getRecipientCity();

            assertEquals(getParseError(file), recipientCity, parsedCity);
        });
    }

    @Test
    public void parseStreet() {

        getDeliveryNotes().forEach(file -> {
            String recipientStreet = "Abweichgasse 3";

            String parsedStreet = parseSingleLabel(file).getRecipientStreet();

            assertEquals(getParseError(file), recipientStreet, parsedStreet);
        });
    }


    @Test
    public void parseCountryCode() {

        getDeliveryNotes().forEach(file -> {
            String countryCode = "AT";

            String parsedCountry = parseSingleLabel(file).getRecipientCountryCode();

            assertEquals(getParseError(file), countryCode, parsedCountry);
        });
    }

    @Test
    public void parsePersonOfContact() {
        String nameOfPerson = "Frau Maria Müller";
        getDeliveryNotesWithPersonOfContact().forEach(file -> {

            Optional<String> parsedName = parseSingleLabel(file).getPersonOfContact();

            assertThat(getParseError(file), parsedName, hasValue(nameOfPerson));
        });

    }

    @Test
    public void parseCompanyName() {
        String nameOfCompany = "Musterfirma 2";

        getDeliveryNotes().forEach(file -> {
            String parsedName = parseSingleLabel(file).getRecipientName();

            assertEquals(getParseError(file), nameOfCompany, parsedName);
        });
    }
}

