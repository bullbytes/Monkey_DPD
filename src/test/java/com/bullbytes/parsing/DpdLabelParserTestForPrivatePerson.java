package com.bullbytes.parsing;

import com.bullbytes.TestUtil;
import com.bullbytes.dpdlabel.DpdLabel;
import com.bullbytes.dpdlabel.LabelDefaults;
import com.bullbytes.parsing.deliverynotes.DeliveryNotes;
import com.bullbytes.parsing.deliverynotes.DeliveryNotesForPrivatePerson;
import com.bullbytes.parsing.deliverynotes.DeliveryNotesForPrivatePersonWithOrderNr;
import com.bullbytes.parsing.parsers.DpdLabelParser;
import com.bullbytes.utils.AcademicTitles;
import com.bullbytes.utils.Strings;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.List;
import java.util.Optional;

import static com.bullbytes.TestUtil.getParseError;
import static com.bullbytes.TestUtil.parseSingleLabel;
import static com.github.npathai.hamcrestopt.OptionalMatchers.hasValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.*;

/**
 * Tests whether the {@link DpdLabelParser} can parse a delivery note for a private person (as opposed to a
 * delivery note company).
 * <p>
 * Created by Matthias Braun on 12/08/16.
 */
public final class DpdLabelParserTestForPrivatePerson {

    private static final Logger log = LoggerFactory.getLogger(DpdLabelParserTestForPrivatePerson.class);

    private static boolean isDigit(char character) {
        return character >= '0' && character <= '9';
    }

    private static boolean consistsOfDigits(String text) {
        return text.chars()
                .mapToObj(i -> (char) i)
                .allMatch(DpdLabelParserTestForPrivatePerson::isDigit);
    }

    private static void checkThatLabelHasFields(DpdLabel label, File testFile) {
        String recipientName = label.getRecipientName();
        String city = label.getRecipientCity();
        String postalCode = label.getRecipientPostalCode();
        String customerNr = label.getCustomerNr();
        String countryCode = label.getRecipientCountryCode();
        String street = label.getRecipientStreet();

        assertFalse("We assume the recipient's name is not empty. File: " + testFile, recipientName.isEmpty());

        assertFalse("We assume the recipient's city is not empty. File: " + testFile, city.isEmpty());
        assertNotEquals("We assume the recipient's city is not the default one. File: " + testFile, LabelDefaults.CITY, city);

        assertTrue("We assume the recipient's postal code consists of digits. File: " + testFile, consistsOfDigits(postalCode));
        assertNotEquals("We assume the recipient's postal code is not the default one. File: " + testFile, LabelDefaults.POSTAL_CODE, postalCode);

        assertFalse("We assume the recipient's customer number is not empty. File: " + testFile, customerNr.isEmpty());
        assertNotEquals("We assume the recipient's customer number is not the default one. File: " + testFile, LabelDefaults.CUSTOMER_NR, customerNr);

        assertEquals("We assume the recipient's country code is two characters long. File: " + testFile, 2, countryCode.length());
        // There are actually notes that have no country so it's legit to use the default country code since it results in a correctly parsed note

        assertFalse("We assume the recipient's street is not empty. File: " + testFile, street.isEmpty());

        label.getOrderNr().ifPresent(orderNr -> assertFalse("The order number should not be empty. File: " + testFile, orderNr.isEmpty()));

        label.getAcademicTitle().ifPresent(title -> assertTrue(
                "We assume that the recipient's title ('" + title + "') is among the known set of titles. File: " + testFile,
                Strings.containsIgnoreCase(AcademicTitles.getKnownAbbreviatedTitles(), title)));

        label.getPersonOfContact().ifPresent(
                personOfContact -> assertFalse("The person of contact should not be empty. File: " + testFile, personOfContact.isEmpty()));
    }

    @Test
    public void parseOrderNr() {
        String orderNr = "123";

        DeliveryNotesForPrivatePerson.getNotesWithOrderNr().forEach(file -> {

            Optional<String> parsedOrderNr = parseSingleLabel(file).getOrderNr();
            assertThat(parsedOrderNr, hasValue(orderNr));

        });
    }


    @Test
    public void parseNoteWithTitleAndSalutation() {

        File file = DeliveryNotesForPrivatePersonWithOrderNr.getDeliveryNoteWithAcademicTitleAndSalutation();
        DpdLabel label = parseSingleLabel(file);
        Optional<String> parsedTitle = label.getAcademicTitle();
        Optional<String> parsedSalutation = label.getSalutation();
        Optional<String> personOfContact = label.getPersonOfContact();

        String expectedTitle = "Dr.";
        assertThat(parsedTitle, hasValue(expectedTitle));
        String expectedSalutation = "Herr";
        assertThat(parsedSalutation, hasValue(expectedSalutation));

        // There was a bug in the parser where we set the title as the person of contact
        assertFalse(personOfContact.isPresent());
    }

    @Test
    public void parseAcademicTitle() {
        String title = "Dr.";
        DeliveryNotesForPrivatePerson.getNotesWithAcademicTitle().forEach(file -> {

            Optional<String> parsedTitle = parseSingleLabel(file).getAcademicTitle();

            assertThat(parsedTitle, hasValue(title));
        });
    }

    @Test
    public void parseSalutation() {
        String salutation = "Herr";
        DeliveryNotesForPrivatePerson.getNotesWithSalutation().forEach(file -> {

            Optional<String> parsedSalutation = parseSingleLabel(file).getSalutation();

            assertThat(parsedSalutation, hasValue(salutation));
        });
    }

    @Test
    public void orderNrIsEmptyWhenParsingNoteWithoutOrderNr() {

        DeliveryNotesForPrivatePerson.getNotesWithoutOrderNr().forEach(deliveryNote -> {
            Optional<String> parsedOrderNr = parseSingleLabel(deliveryNote).getOrderNr();

            assertEquals(getParseError(deliveryNote), Optional.empty(), parsedOrderNr);
        });
    }

    @Test
    public void parseDeliveryNoteForPersonWithTitleAndSalutation() {
        File file = DeliveryNotesForPrivatePersonWithOrderNr.getDeliveryNoteWithAcademicTitleAndSalutation();
        DpdLabel label = parseSingleLabel(file);
        assertThat(label.getSalutation(), hasValue("Herr"));
        assertThat(label.getAcademicTitle(), hasValue("Dr."));
    }

    @Test
    public void salutationIsEmptyWhenParsingNotesWithoutSalutation() {
        DeliveryNotesForPrivatePerson.getNotesWithoutSalutation().forEach(file -> {

            Optional<String> parsedSalutation = parseSingleLabel(file).getSalutation();

            assertEquals(Optional.empty(), parsedSalutation);
        });
    }

    @Test
    public void parseCustomerNr() {
        String customerNr = "ADR-000004";
        DeliveryNotesForPrivatePerson.getAllNotesWithKnownRecipientData().forEach(file -> {

            String parsedPostalCode = parseSingleLabel(file).getCustomerNr();

            assertEquals(getParseError(file), customerNr, parsedPostalCode);
        });
    }

    @Test
    public void checkAllNotesForValidData() {

        List<File> allNotes = DeliveryNotesForPrivatePerson.getAllNotesWithKnownRecipientData();
        assertFalse("There should be notes to test", allNotes.isEmpty());

        allNotes
                .forEach(file -> {
                    DpdLabel label = parseSingleLabel(file);
                    checkThatLabelHasFields(label, file);
                });
    }

    @Test
    public void parseNoteWithSpaceInCity() {
        File file = DeliveryNotesForPrivatePerson.getNoteWithCityContainingSpaces();
        DpdLabel label = parseSingleLabel(file);
        String parsedRecipientName = label.getRecipientName();
        String parsedCity = label.getRecipientCity();
        String parsedPostalCode = label.getRecipientPostalCode();

        assertEquals("Margarete Schwarz", parsedRecipientName);
        assertEquals("St. Marien", parsedCity);
        assertEquals("4502", parsedPostalCode);
    }

    @Test
    public void parsePostalCode() {

        String recipientPostalCode = "4020";
        DeliveryNotesForPrivatePerson.getAllNotesWithKnownRecipientData().forEach(file -> {

            String parsedPostalCode = parseSingleLabel(file).getRecipientPostalCode();

            assertEquals(getParseError(file), recipientPostalCode, parsedPostalCode);
        });
    }

    @Test
    public void parseCity() {
        String recipientCity = "Linz";

        DeliveryNotesForPrivatePerson.getAllNotesWithKnownRecipientData().forEach(file -> {
            String parsedCity = parseSingleLabel(file).getRecipientCity();

            assertEquals(getParseError(file), recipientCity, parsedCity);
        });
    }

    @Test
    public void parseStreet() {
        String recipientStreet = "Privatstraße 34";

        DeliveryNotesForPrivatePerson.getAllNotesWithKnownRecipientData().forEach(file -> {
            String parsedStreet = parseSingleLabel(file).getRecipientStreet();

            assertEquals(getParseError(file), recipientStreet, parsedStreet);
        });
    }

    @Test
    public void parseCountryCode() {
        String countryCode = "AT";

        DeliveryNotesForPrivatePerson.getAllNotesWithKnownRecipientData().forEach(file -> {
            String parsedCountry = parseSingleLabel(file).getRecipientCountryCode();

            assertEquals(getParseError(file), countryCode, parsedCountry);
        });
    }

    @Test
    public void parseEmptyPdf() {
        File emptyPdf = TestUtil.load("/empty.pdf");
        DpdLabel parsedLabel = parseSingleLabel(emptyPdf);
        assertFalse(parsedLabel == null);
    }

    @Test
    public void parseName() {
        String nameOfPerson = "Paul Privatperson";

        DeliveryNotesForPrivatePerson.getAllNotesWithKnownRecipientData().forEach(file -> {
            DpdLabel label = parseSingleLabel(file);
            String parsedName = label.getRecipientName();

            assertEquals(getParseError(file), nameOfPerson, parsedName);
        });
    }

}