package com.bullbytes.parsing.deliverynotes;

/**
 * Helps dealing testing the parsing of delivery notes.
 * <p>
 * Created by Matthias Braun on 07/09/16.
 */
public final class DeliveryNotes {

    public static final String DELIVERY_NOTES_DIR = "/delivery notes/";

    private DeliveryNotes() {
    }
}
