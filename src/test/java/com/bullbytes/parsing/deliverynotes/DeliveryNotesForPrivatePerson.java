package com.bullbytes.parsing.deliverynotes;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.bullbytes.parsing.deliverynotes.DeliveryNotes.DELIVERY_NOTES_DIR;

/**
 * Provides delivery notes from MonKey Office for private persons. Used for unit tests.
 * <p>
 * Created by Matthias Braun on 01/09/16.
 */
public final class DeliveryNotesForPrivatePerson {
    private static final String PRIVATE_PERSONS_DIR = "for private persons/";
    static final String DELIVERY_NOTES_FOR_PRIVATE_PERSONS_DIR = DELIVERY_NOTES_DIR + PRIVATE_PERSONS_DIR;

    private DeliveryNotesForPrivatePerson() {
    }

    public static List<File> getNotesWithoutSalutation() {
        return Arrays.asList(
                DeliveryNotesForPrivatePersonWithOrderNr.getDeliveryNoteWithAcademicTitleAndNoSalutation(),
                DeliveryNotesForPrivatePersonWithOrderNr.getDeliveryNoteWithoutAcademicTitleAndNoSalutation()
        );
    }

    public static List<File> getNotesWithAcademicTitle() {
        return Arrays.asList(
                DeliveryNotesForPrivatePersonWithOrderNr.getDeliveryNoteWithAcademicTitleAndSalutation(),
                DeliveryNotesForPrivatePersonWithOrderNr.getDeliveryNoteWithAcademicTitleAndNoSalutation(),
                DeliveryNotesForPrivatePersonWithoutOrderNr.getNoteWithAcademicTitleAndSalutation()
        );
    }

    public static List<File> getNotesWithSalutation() {
        return Arrays.asList(
                DeliveryNotesForPrivatePersonWithOrderNr.getDeliveryNoteWithSalutationAndNoAcademicTitle(),
                DeliveryNotesForPrivatePersonWithOrderNr.getDeliveryNoteWithAcademicTitleAndSalutation(),
                DeliveryNotesForPrivatePersonWithoutOrderNr.getNoteWithoutTitleAndWithSalutation(),
                DeliveryNotesForPrivatePersonWithoutOrderNr.getNoteWithAcademicTitleAndSalutation()
        );
    }

    public static List<File> getNotesWithOrderNr() {
        return Arrays.asList(
                DeliveryNotesForPrivatePersonWithOrderNr.getDeliveryNoteWithAcademicTitleAndNoSalutation(),
                DeliveryNotesForPrivatePersonWithOrderNr.getDeliveryNoteWithSalutationAndNoAcademicTitle(),
                DeliveryNotesForPrivatePersonWithOrderNr.getDeliveryNoteWithoutAcademicTitleAndNoSalutation(),
                DeliveryNotesForPrivatePersonWithOrderNr.getDeliveryNoteWithAcademicTitleAndSalutation()
        );
    }

    public static List<File> getNotesWithoutOrderNr() {
        return DeliveryNotesForPrivatePersonWithoutOrderNr.getAllNotes();
    }

    /**
     * @return all delivery notes where all the recipient data is known and the same
     */
    public static List<File> getAllNotesWithKnownRecipientData() {
        List<File> allNotes = new ArrayList<>();
        allNotes.addAll(DeliveryNotesForPrivatePersonWithOrderNr.getAllNotes());
        allNotes.addAll(DeliveryNotesForPrivatePersonWithoutOrderNr.getAllNotes());
        // The data of this delivery note is different from the rest
        allNotes.remove(getNoteWithCityContainingSpaces());
        return allNotes;
    }

    public static File getNoteWithCityContainingSpaces() {
        return DeliveryNotesForPrivatePersonWithoutOrderNr.getNoteWithCityContainingSpaces();
    }
}
