package com.bullbytes.parsing.deliverynotes;

import com.bullbytes.TestUtil;

import java.io.File;
import java.util.List;

/**
 * These delivery notes are for private persons and have an order number.
 * <p>
 * Created by Matthias Braun on 01/09/16.
 */
public final class DeliveryNotesForPrivatePersonWithOrderNr {

    private static final String WITH_ORDER_NUMBER_DIR = "with order number";
    private static final String NOTES_FOR_PRIVATE_PERSONS_WITH_ORDER_NR_DIR =
            DeliveryNotesForPrivatePerson.DELIVERY_NOTES_FOR_PRIVATE_PERSONS_DIR + WITH_ORDER_NUMBER_DIR;

    private DeliveryNotesForPrivatePersonWithOrderNr() {
    }


    static File getDeliveryNoteWithSalutationAndNoAcademicTitle() {
        String fileName = NOTES_FOR_PRIVATE_PERSONS_WITH_ORDER_NR_DIR + "/no title, with salutation.pdf";
        return TestUtil.load(fileName);
    }

    static File getDeliveryNoteWithAcademicTitleAndNoSalutation() {
        String fileName = NOTES_FOR_PRIVATE_PERSONS_WITH_ORDER_NR_DIR + "/with title, no salutation.pdf";
        return TestUtil.load(fileName);
    }

    public static File getDeliveryNoteWithAcademicTitleAndSalutation() {
        String fileName = NOTES_FOR_PRIVATE_PERSONS_WITH_ORDER_NR_DIR + "/with title and salutation.pdf";
        return TestUtil.load(fileName);
    }

    static File getDeliveryNoteWithoutAcademicTitleAndNoSalutation() {
        String fileName = NOTES_FOR_PRIVATE_PERSONS_WITH_ORDER_NR_DIR + "/no title, no salutation.pdf";
        return TestUtil.load(fileName);
    }

    static List<File> getAllNotes() {
        return TestUtil.getFilesInDirectory(NOTES_FOR_PRIVATE_PERSONS_WITH_ORDER_NR_DIR);
    }
}
