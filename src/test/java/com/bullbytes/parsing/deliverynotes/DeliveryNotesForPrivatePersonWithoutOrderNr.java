package com.bullbytes.parsing.deliverynotes;

import com.bullbytes.TestUtil;

import java.io.File;
import java.util.List;


/**
 * These delivery notes are for a private person and lack an order number.
 * <p>
 * Created by Matthias Braun on 01/09/16.
 */
final class DeliveryNotesForPrivatePersonWithoutOrderNr {

    private static final String WITHOUT_ORDER_NUMBER_DIR = "without order number/";
    private static final String NOTES_FOR_PRIVATE_PERSONS_WITHOUT_ORDER_NR_DIR =
            DeliveryNotesForPrivatePerson.DELIVERY_NOTES_FOR_PRIVATE_PERSONS_DIR + WITHOUT_ORDER_NUMBER_DIR;

    private DeliveryNotesForPrivatePersonWithoutOrderNr() {
    }

    static File getNoteWithoutTitleAndWithSalutation() {
        String fileName = NOTES_FOR_PRIVATE_PERSONS_WITHOUT_ORDER_NR_DIR + "no title, with salutation.pdf";
        return TestUtil.load(fileName);
    }

    static File getNoteWithAcademicTitleAndSalutation() {
        String fileName = NOTES_FOR_PRIVATE_PERSONS_WITHOUT_ORDER_NR_DIR + "with title and salutation.pdf";
        return TestUtil.load(fileName);
    }

    static List<File> getAllNotes() {
        return TestUtil.getFilesInDirectory(NOTES_FOR_PRIVATE_PERSONS_WITHOUT_ORDER_NR_DIR);
    }

    static File getNoteWithCityContainingSpaces() {
        String fileName = NOTES_FOR_PRIVATE_PERSONS_WITHOUT_ORDER_NR_DIR + "with spaces in city.pdf";

        return TestUtil.load(fileName);
    }
}
