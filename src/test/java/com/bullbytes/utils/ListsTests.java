package com.bullbytes.utils;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;


/**
 * Tests the {@link Lists}.
 * <p>
 * Created by Matthias Braun on 12/08/16.
 */
public final class ListsTests {

    @Test
    public void iterateWithZeroStartIndex() {
        List<String> list = Arrays.asList("One", "Two", "Three");
        Lists.forEachWithIndex(list, (index, string) -> {
            if (index == 0) {
                assertEquals(string, "One");
            } else if (index == 1) {
                assertEquals(string, "Two");
            } else if (index == 2) {
                assertEquals(string, "Three");
            }
        });
    }
}
