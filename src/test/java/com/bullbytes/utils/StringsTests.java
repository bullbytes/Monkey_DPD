package com.bullbytes.utils;

import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.*;

/**
 * Tests the {@link Strings} class.
 * <p>
 * Created by Matthias Braun on 12/08/16.
 */
public final class StringsTests {
    @Test
    public void subAfterLastCharacterOfStringThatEndsWithCharacterIsTheEmptyString() {
        String input = "Some ! text !";
        Optional<String> output = Strings.getSubAfterLast(input, "!");
        assertTrue(output.isPresent());
        assertEquals("", output.get());
    }

    @Test
    public void getSubBeforeFirstSingleCharacterString() {
        String input = "my text#other !! other text";
        Optional<String> output = Strings.getSubBeforeFirst(input, "#");
        assertTrue(output.isPresent());
        assertEquals("my text", output.get());
    }

    @Test
    public void getSubBeforeFirstMultiCharacterString() {
        String input = "text!!other !! other text";
        Optional<String> output = Strings.getSubBeforeFirst(input, "!!");
        assertTrue(output.isPresent());
        assertEquals("text", output.get());
    }

    @Test
    public void getSubAfterLastMultiCharacterString() {
        String input = "!Some !!text";
        Optional<String> output = Strings.getSubAfterLast(input, "!!");
        assertTrue(output.isPresent());
        assertEquals("text", output.get());
    }

    @Test
    public void getSubAfterLastSingleCharacterString() {
        String input = "ßSome ßtext";
        Optional<String> output = Strings.getSubAfterLast(input, "ß");
        assertTrue(output.isPresent());
        assertEquals("text", output.get());
    }

    @Test
    public void getSubAfterLastCharacterOfEmptyStringIsEmptyOptional() {
        String input = "";
        Optional<String> output = Strings.getSubAfterLast(input, "ß");
        assertFalse(output.isPresent());
    }

}